
-- Auto generated at 2024-10-27 14:58:25 UTC+07:00
return {
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Employee Badge A",
		localized_name = "info.tf2pm.hat.employee_badge_a",
		localized_description = "info.tf2pm.hat.employee_badge_a_desc",
		icon = "backpack/player/items/all_class/id_badge_gold",
		models = {
			["basename"] = "models/player/items/all_class/id_badge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Employee Badge B",
		localized_name = "info.tf2pm.hat.employee_badge_b",
		localized_description = "info.tf2pm.hat.employee_badge_b_desc",
		icon = "backpack/player/items/all_class/id_badge_silver",
		models = {
			["basename"] = "models/player/items/all_class/id_badge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Employee Badge C",
		localized_name = "info.tf2pm.hat.employee_badge_c",
		localized_description = "info.tf2pm.hat.employee_badge_c_desc",
		icon = "backpack/player/items/all_class/id_badge_bronze",
		models = {
			["basename"] = "models/player/items/all_class/id_badge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Employee Badge Plat",
		localized_name = "info.tf2pm.hat.employee_badge_plat",
		localized_description = "info.tf2pm.hat.employee_badge_plat_desc",
		icon = "backpack/player/items/all_class/id_badge_platinum",
		models = {
			["basename"] = "models/player/items/all_class/id_badge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Duel Medal Bronze",
		description = "duel_medal",
		localized_name = "info.tf2pm.hat.duel_medal_bronze",
		localized_description = "info.tf2pm.hat.duel_medal_bronze_desc",
		icon = "backpack/player/items/all_class/dueling_medal_bronze",
		models = {
			["basename"] = "models/player/items/all_class/dueling_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Duel Medal Silver",
		description = "duel_medal",
		localized_name = "info.tf2pm.hat.duel_medal_silver",
		localized_description = "info.tf2pm.hat.duel_medal_silver_desc",
		icon = "backpack/player/items/all_class/dueling_medal_silver",
		models = {
			["basename"] = "models/player/items/all_class/dueling_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Duel Medal Gold",
		description = "duel_medal",
		localized_name = "info.tf2pm.hat.duel_medal_gold",
		localized_description = "info.tf2pm.hat.duel_medal_gold_desc",
		icon = "backpack/player/items/all_class/dueling_medal_gold",
		models = {
			["basename"] = "models/player/items/all_class/dueling_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Duel Medal Plat",
		description = "duel_medal",
		localized_name = "info.tf2pm.hat.duel_medal_plat",
		localized_description = "info.tf2pm.hat.duel_medal_plat_desc",
		icon = "backpack/player/items/all_class/dueling_medal_platinum",
		models = {
			["basename"] = "models/player/items/all_class/dueling_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Polycount Pin",
		localized_name = "info.tf2pm.hat.polycount_pin",
		localized_description = "info.tf2pm.hat.polycount_pin_desc",
		icon = "backpack/player/items/all_class/polypack_badge",
		models = {
			["basename"] = "models/player/items/all_class/polypack_badge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "TTG Badge",
		localized_name = "info.tf2pm.hat.ttg_badge",
		localized_description = "info.tf2pm.hat.ttg_badge_desc",
		icon = "backpack/player/items/all_class/ttg_badge",
		models = {
			["basename"] = "models/player/items/all_class/ttg_badge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Portal 2 Pin",
		localized_name = "info.tf2pm.hat.portal2_pin",
		icon = "backpack/player/items/all_class/p2_pin",
		models = {
			["basename"] = "models/player/items/all_class/p2_pin.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Resurrection Associate Pin",
		localized_name = "info.tf2pm.hat.resurrection_associate_pin",
		icon = "backpack/player/items/all_class/p2_pin",
		models = {
			["basename"] = "models/player/items/all_class/p2_pin.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "SpaceChem Pin",
		localized_name = "info.tf2pm.hat.spacechem_pin",
		localized_description = "info.tf2pm.hat.spacechem_pin_desc",
		icon = "backpack/player/items/all_class/spacechem_pin",
		models = {
			["basename"] = "models/player/items/all_class/spacechem_pin.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Dr. Grordbort's Crest",
		localized_name = "info.tf2pm.hat.drgrordbortbadge",
		localized_description = "info.tf2pm.hat.drgrordbortbadge_desc",
		icon = "backpack/player/items/all_class/drg_badge",
		models = {
			["basename"] = "models/player/items/all_class/drg_badge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "TournamentMedal - GWJ Winners",
		localized_name = "info.tf2pm.hat.tournamentmedal_gwj_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_winners_desc",
		icon = "backpack/player/items/all_class/weiner_bomb_medal",
		models = {
			["basename"] = "models/player/items/all_class/weiner_bomb_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "TournamentMedal - GWJ Runnerups",
		localized_name = "info.tf2pm.hat.tournamentmedal_gwj_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_runnerups_desc",
		icon = "backpack/player/items/all_class/weiner_bomb_run",
		models = {
			["basename"] = "models/player/items/all_class/weiner_bomb_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "TournamentMedal - GWJ Participants",
		localized_name = "info.tf2pm.hat.tournamentmedal_gwj_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_participants_desc",
		icon = "backpack/player/items/all_class/weiner_bomb_part",
		models = {
			["basename"] = "models/player/items/all_class/weiner_bomb_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "TournamentMedal - ETF2LHL Winners",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2lhl_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_winners_desc",
		icon = "backpack/player/items/all_class/etf2l_highlander_medal",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "TournamentMedal - ETF2LHL 2nd",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2lhl_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_2nd_desc",
		icon = "backpack/player/items/all_class/etf2l_highlander_medal_1",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "TournamentMedal - ETF2LHL 3rd",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2lhl_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_3rd_desc",
		icon = "backpack/player/items/all_class/etf2l_highlander_medal_2",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "TournamentMedal - ETF2LHL Participants",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2lhl_4th",
		localized_description = "info.tf2pm.hat.tournamentmedal_participants_desc",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "TournamentMedal - UGCHL Participants",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_participants_desc",
		icon = "backpack/player/items/all_class/ugc_highlander_participant",
		models = {
			["basename"] = "models/player/items/all_class/ugc_highlander_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "TournamentMedal - UGCHLDiv1 Winners",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_platinum_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_winners_desc",
		icon = "backpack/player/items/all_class/ugc_highlander_platinum",
		models = {
			["basename"] = "models/player/items/all_class/ugc_highlander_platinum.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "TournamentMedal - UGCHLDiv1 2nd",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_platinum_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_2nd_desc",
		icon = "backpack/player/items/all_class/ugc_highlander_platinum_1",
		models = {
			["basename"] = "models/player/items/all_class/ugc_highlander_platinum.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "TournamentMedal - UGCHLDiv1 3rd",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_platinum_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_3rd_desc",
		icon = "backpack/player/items/all_class/ugc_highlander_platinum_2",
		models = {
			["basename"] = "models/player/items/all_class/ugc_highlander_platinum.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "TournamentMedal - UGCHLDiv2 Winners",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_silver_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_winners_desc",
		icon = "backpack/player/items/all_class/ugc_highlander_silver",
		models = {
			["basename"] = "models/player/items/all_class/ugc_highlander_silver.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "TournamentMedal - UGCHLDiv2 2nd",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_silver_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_2nd_desc",
		icon = "backpack/player/items/all_class/ugc_highlander_silver_1",
		models = {
			["basename"] = "models/player/items/all_class/ugc_highlander_silver.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "TournamentMedal - UGCHLDiv2 3rd",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_silver_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_3rd_desc",
		icon = "backpack/player/items/all_class/ugc_highlander_silver_2",
		models = {
			["basename"] = "models/player/items/all_class/ugc_highlander_silver.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "TournamentMedal - UGCHLDiv3 Winners",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_iron_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_winners_desc",
		icon = "backpack/player/items/all_class/ugc_highlander_iron",
		models = {
			["basename"] = "models/player/items/all_class/ugc_highlander_iron.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "TournamentMedal - UGCHLDiv3 2nd",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_iron_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_2nd_desc",
		icon = "backpack/player/items/all_class/ugc_highlander_iron_1",
		models = {
			["basename"] = "models/player/items/all_class/ugc_highlander_iron.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "TournamentMedal - UGCHLDiv3 3rd",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_iron_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_3rd_desc",
		icon = "backpack/player/items/all_class/ugc_highlander_iron_2",
		models = {
			["basename"] = "models/player/items/all_class/ugc_highlander_iron.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Bombinomicon",
		localized_name = "info.tf2pm.hat.bombinomicon_badge",
		localized_description = "info.tf2pm.hat.bombinomicon_badge_desc",
		icon = "backpack/player/items/all_class/bombonomicon",
		models = {
			["basename"] = "models/player/items/all_class/bombonomicon.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Mark of the Saint",
		localized_name = "info.tf2pm.hat.saint_pin",
		icon = "backpack/player/items/all_class/sr3_badge",
		models = {
			["basename"] = "models/player/items/all_class/sr3_badge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Dr. Grordbort's Copper Crest",
		localized_name = "info.tf2pm.hat.drg_copperbadge",
		localized_description = "info.tf2pm.hat.drg_copperbadge_desc",
		icon = "backpack/player/items/all_class/drg_badge_copper",
		models = {
			["basename"] = "models/player/items/all_class/drg_badge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Photo Badge",
		description = "can_customize_texture",
		localized_name = "info.tf2pm.hat.photobadge",
		localized_description = "info.tf2pm.hat.photobadge_desc",
		icon = "backpack/player/items/all_class/photo_badge",
		models = {
			["basename"] = "models/player/items/all_class/photo_badge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Clan Pride",
		description = "can_customize_texture",
		localized_name = "info.tf2pm.hat.stampablemedal",
		localized_description = "info.tf2pm.hat.stampablemedal_desc",
		icon = "backpack/player/items/all_class/stampable_medal",
		models = {
			["basename"] = "models/player/items/all_class/stampable_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Dr. Grordbort's Silver Crest",
		localized_name = "info.tf2pm.hat.drg_silverbadge",
		localized_description = "info.tf2pm.hat.drg_silverbadge_desc",
		icon = "backpack/player/items/all_class/drg_badge_silver",
		models = {
			["basename"] = "models/player/items/all_class/drg_badge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Spirit Of Giving",
		description = "duel_medal",
		localized_name = "info.tf2pm.hat.gifting_badge",
		localized_description = "info.tf2pm.hat.gifting_badge_desc",
		icon = "backpack/player/items/all_class/xms_allclass_giftbadge",
		models = {
			["basename"] = "models/player/items/all_class/xms_allclass_giftbadge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Tournament Fall 2011 - Platinum 1st Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_platinum_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_fall2011",
		icon = "backpack/player/items/all_class/ugc_trophy_medal_platinum",
		models = {
			["basename"] = "models/player/items/all_class/ugc_trophy_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Tournament Fall 2011 - Platinum 2nd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_platinum_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_fall2011",
		icon = "backpack/player/items/all_class/ugc_trophy_medal_platinum",
		models = {
			["basename"] = "models/player/items/all_class/ugc_trophy_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Tournament Fall 2011 - Platinum 3rd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_platinum_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_fall2011",
		icon = "backpack/player/items/all_class/ugc_trophy_medal_platinum",
		models = {
			["basename"] = "models/player/items/all_class/ugc_trophy_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Tournament Fall 2011 - Platinum Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_platinum_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_fall2011",
		icon = "backpack/player/items/all_class/ugc_trophy_medal_platinum",
		models = {
			["basename"] = "models/player/items/all_class/ugc_trophy_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Tournament Autumn 2011 - Euro Platinum",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_europlatinum",
		localized_description = "info.tf2pm.hat.tournamentmedal_autumn2011",
		icon = "backpack/player/items/all_class/ugc_trophy_medal_platinum",
		models = {
			["basename"] = "models/player/items/all_class/ugc_trophy_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Tournament Fall 2011 - Silver 1st Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_silver_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_fall2011",
		icon = "backpack/player/items/all_class/ugc_trophy_medal_silver",
		models = {
			["basename"] = "models/player/items/all_class/ugc_trophy_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Tournament Fall 2011 - Silver 2nd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_silver_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_fall2011",
		icon = "backpack/player/items/all_class/ugc_trophy_medal_silver",
		models = {
			["basename"] = "models/player/items/all_class/ugc_trophy_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Tournament Fall 2011 - Silver 3rd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_silver_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_fall2011",
		icon = "backpack/player/items/all_class/ugc_trophy_medal_silver",
		models = {
			["basename"] = "models/player/items/all_class/ugc_trophy_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Tournament Fall 2011 - Silver Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_silver_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_fall2011",
		icon = "backpack/player/items/all_class/ugc_trophy_medal_silver",
		models = {
			["basename"] = "models/player/items/all_class/ugc_trophy_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Tournament Autumn 2011 - Euro Silver",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_eurosilver",
		localized_description = "info.tf2pm.hat.tournamentmedal_autumn2011",
		icon = "backpack/player/items/all_class/ugc_trophy_medal_silver",
		models = {
			["basename"] = "models/player/items/all_class/ugc_trophy_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Tournament Fall 2011 - Iron 2nd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_iron_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_fall2011",
		icon = "backpack/player/items/all_class/ugc_trophy_medal_participant",
		models = {
			["basename"] = "models/player/items/all_class/ugc_trophy_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Tournament Fall 2011 - Iron 1st Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_iron_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_fall2011",
		icon = "backpack/player/items/all_class/ugc_trophy_medal_participant",
		models = {
			["basename"] = "models/player/items/all_class/ugc_trophy_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Tournament Fall 2011 - Iron 3rd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_iron_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_fall2011",
		icon = "backpack/player/items/all_class/ugc_trophy_medal_participant",
		models = {
			["basename"] = "models/player/items/all_class/ugc_trophy_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Tournament Autumn 2011 - Euro Iron",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_euroiron",
		localized_description = "info.tf2pm.hat.tournamentmedal_autumn2011",
		icon = "backpack/player/items/all_class/ugc_trophy_medal_participant",
		models = {
			["basename"] = "models/player/items/all_class/ugc_trophy_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Tournament Fall 2011 - Tin 1st Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_tin_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_fall2011",
		icon = "backpack/player/items/all_class/ugc_trophy_medal_participant",
		models = {
			["basename"] = "models/player/items/all_class/ugc_trophy_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Tournament Fall 2011 - Tin 2nd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_tin_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_fall2011",
		icon = "backpack/player/items/all_class/ugc_trophy_medal_participant",
		models = {
			["basename"] = "models/player/items/all_class/ugc_trophy_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Tournament Fall 2011 - Tin 3rd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_tin_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_fall2011",
		icon = "backpack/player/items/all_class/ugc_trophy_medal_participant",
		models = {
			["basename"] = "models/player/items/all_class/ugc_trophy_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Tournament Fall 2011 - Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_fall2011",
		icon = "backpack/player/items/all_class/ugc_trophy_medal_participant",
		models = {
			["basename"] = "models/player/items/all_class/ugc_trophy_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Tournament Autumn 2011 - Euro Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_europarticipant",
		localized_description = "info.tf2pm.hat.tournamentmedal_autumn2011",
		icon = "backpack/player/items/all_class/ugc_trophy_medal_participant",
		models = {
			["basename"] = "models/player/items/all_class/ugc_trophy_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "The Bolgan Family Crest",
		localized_name = "info.tf2pm.hat.reckoningbadge",
		localized_description = "info.tf2pm.hat.reckoningbadge_desc",
		icon = "backpack/player/items/all_class/all_reckoning_badge",
		models = {
			["basename"] = "models/player/items/all_class/all_reckoning_badge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = true,
		name = "The Map Maker's Medallion",
		localized_name = "info.tf2pm.hat.mappermedal",
		icon = "backpack/player/items/all_class/mapper_medal",
		models = {
			["basename"] = "models/player/items/all_class/mapper_medal.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "The Merc Medal",
		localized_name = "info.tf2pm.hat.jag_badge",
		localized_description = "info.tf2pm.hat.jag_badge_desc",
		icon = "backpack/player/items/all_class/jag_badge",
		models = {
			["basename"] = "models/player/items/all_class/jag_badge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Tour of Duty Badge Advanced 1",
		localized_name = "info.tf2pm.hat.mvm_badge_advanced1",
		localized_description = "info.tf2pm.hat.mvm_badge_advanced1_desc",
		icon = "backpack/player/items/mvm_loot/all_class/mvm_badge",
		models = {
			["basename"] = "models/player/items/mvm_loot/all_class/mvm_badge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = true,
		name = "The Atomic Accolade",
		localized_name = "info.tf2pm.hat.qc_badge",
		localized_description = "info.tf2pm.hat.qc_badge_desc",
		icon = "backpack/player/items/all_class/qc_badge",
		models = {
			["basename"] = "models/player/items/all_class/qc_badge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Awesomenauts Badge",
		localized_name = "info.tf2pm.hat.awes_medal",
		localized_description = "info.tf2pm.hat.awes_medal_desc",
		icon = "backpack/player/items/all_class/awes_badge",
		models = {
			["basename"] = "models/player/items/all_class/awes_badge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "The Vigilant Pin",
		localized_name = "info.tf2pm.hat.xcom_pin",
		localized_description = "info.tf2pm.hat.xcom_pin_desc",
		icon = "backpack/player/items/all_class/xcom_badge",
		models = {
			["basename"] = "models/player/items/all_class/xcom_badge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "The Friends Forever Companion Square Badge",
		localized_name = "info.tf2pm.hat.companionsquare_badge",
		localized_description = "info.tf2pm.hat.companionsquare_badge_desc",
		icon = "backpack/player/items/all_class/3a_cube",
		models = {
			["basename"] = "models/player/items/all_class/3a_cube.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "The Triple A Badge",
		localized_name = "info.tf2pm.hat.threea_badge",
		localized_description = "info.tf2pm.hat.threea_badge_desc",
		icon = "backpack/player/items/all_class/3a_badge",
		models = {
			["basename"] = "models/player/items/all_class/3a_badge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "The Heroic Companion Badge",
		localized_name = "info.tf2pm.hat.coh2_badge",
		localized_description = "info.tf2pm.hat.coh2_badge_desc",
		icon = "backpack/player/items/all_class/coh_badge_sovjet",
		models = {
			["basename"] = "models/player/items/all_class/coh_badge_sovjet.mdl",
		},
		styles = {
			[0] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.coh2_badge_style1",
				models = {
					["basename"] = "models/player/items/all_class/coh_badge_sovjet.mdl",
				}
			},
			[1] = {
				responsive_skins = {0, 1},
				localized_name = "info.tf2pm.hat.coh2_badge_style2",
				models = {
					["basename"] = "models/player/items/all_class/coh_badge_german.mdl",
				}
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Tour of Duty Badge Intermediate 1",
		localized_name = "info.tf2pm.hat.mvm_badge_intermediate1",
		localized_description = "info.tf2pm.hat.mvm_badge_intermediate1_desc",
		icon = "backpack/player/items/mvm_loot/all_class/mvm_badge_rust",
		models = {
			["basename"] = "models/player/items/mvm_loot/all_class/mvm_badge_rust.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Tour of Duty Badge Expert 1",
		localized_name = "info.tf2pm.hat.mvm_badge_expert1",
		localized_description = "info.tf2pm.hat.mvm_badge_expert1_desc",
		icon = "backpack/player/items/mvm_loot/all_class/mvm_badge_diamond",
		models = {
			["basename"] = "models/player/items/mvm_loot/all_class/mvm_badge_diamond.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "The Whale Bone Charm",
		localized_name = "info.tf2pm.hat.dishonored_badge",
		localized_description = "info.tf2pm.hat.dishonored_badge_desc",
		icon = "backpack/workshop_partner/player/items/all_class/dishonored_badge/dishonored_badge",
		models = {
			["basename"] = "models/workshop_partner/player/items/all_class/dishonored_badge/dishonored_badge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "The Hitt Mann Badge",
		localized_name = "info.tf2pm.hat.hm_badge",
		localized_description = "info.tf2pm.hat.hm_badge_desc",
		icon = "backpack/workshop_partner/player/items/all_class/hm_badge/hm_badge",
		models = {
			["basename"] = "models/workshop_partner/player/items/all_class/hm_badge/hm_badge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "The Saxxy Clapper Badge",
		localized_name = "info.tf2pm.hat.clacker2012",
		localized_description = "info.tf2pm.hat.clacker2012_desc",
		icon = "backpack/player/items/all_class/all_clacker_badge",
		models = {
			["basename"] = "models/player/items/all_class/all_clacker_badge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Faerie Solitaire Pin",
		localized_name = "info.tf2pm.hat.faerie_solitaire_pin",
		localized_description = "info.tf2pm.hat.faerie_solitaire_pin_desc",
		icon = "backpack/player/items/all_class/all_fs_badge",
		models = {
			["basename"] = "models/player/items/all_class/all_fs_badge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Tour of Duty Badge Advanced 2",
		localized_name = "info.tf2pm.hat.mvm_badge_advanced2",
		localized_description = "info.tf2pm.hat.mvm_badge_advanced2_desc",
		icon = "backpack/player/items/mvm_loot/all_class/mvm_badge_engy",
		models = {
			["basename"] = "models/player/items/mvm_loot/all_class/mvm_badge_engy.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Smissmas Wreath",
		localized_name = "info.tf2pm.hat.wreath_2012",
		localized_description = "info.tf2pm.hat.wreath_2012_desc",
		icon = "backpack/player/items/all_class/all_wreath_badge",
		models = {
			["basename"] = "models/player/items/all_class/all_wreath_badge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Croft's Crest",
		localized_name = "info.tf2pm.hat.croftscrest",
		localized_description = "info.tf2pm.hat.croftscrest_desc",
		icon = "backpack/player/items/all_class/tomb_badge",
		models = {
			["basename"] = "models/player/items/all_class/tomb_badge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Tour of Duty Badge Advanced 3",
		localized_name = "info.tf2pm.hat.mvm_badge_advanced3",
		localized_description = "info.tf2pm.hat.mvm_badge_advanced3_desc",
		icon = "backpack/player/items/mvm_loot/all_class/mvm_badge_shield",
		models = {
			["basename"] = "models/player/items/mvm_loot/all_class/mvm_badge_shield.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "The Baronial Badge",
		localized_name = "info.tf2pm.hat.baronialbadge",
		icon = "backpack/workshop_partner/player/items/all_class/thief_badge/thief_badge",
		models = {
			["basename"] = "models/workshop_partner/player/items/all_class/thief_badge/thief_badge_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "PASS Time Miniature Half JACK",
		localized_name = "info.tf2pm.hat.passtimeminiaturehalfjack",
		localized_description = "info.tf2pm.hat.passtimeminiaturehalfjack_desc",
		icon = "backpack/workshop_partner/player/items/all_class/jackbadge/jackbadge",
		models = {
			["basename"] = "models/workshop_partner/player/items/all_class/jackbadge/jackbadge.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "PASS Time Early Participation Pin",
		localized_name = "info.tf2pm.hat.passtimeearlyparticipationpin",
		localized_description = "info.tf2pm.hat.passtimeearlyparticipationpin_desc",
		icon = "backpack/workshop_partner/player/items/all_class/jackbadge/jackbadge_limited",
		models = {
			["basename"] = "models/workshop_partner/player/items/all_class/jackbadge/jackbadge_limited.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place North American Platinum Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_1st_na_platinum",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6firstplatinum",
		models = {
			["basename"] = "models/player/items/all_class/ugc6first.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place North American Platinum Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_2nd_na_platinum",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6secondplatinum",
		models = {
			["basename"] = "models/player/items/all_class/ugc6second.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place North American Platinum Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_3rd_na_platinum",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6thirdplatinum",
		models = {
			["basename"] = "models/player/items/all_class/ugc6third.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place European Platinum Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_1st_europe_platinum",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6firstplatinum",
		models = {
			["basename"] = "models/player/items/all_class/ugc6first.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place European Platinum Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_2nd_europe_platinum",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6secondplatinum",
		models = {
			["basename"] = "models/player/items/all_class/ugc6second.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place European Platinum Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_3rd_europe_platinum",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6thirdplatinum",
		models = {
			["basename"] = "models/player/items/all_class/ugc6third.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place South American Platinum Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_1st_sa_platinum",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6firstplatinum",
		models = {
			["basename"] = "models/player/items/all_class/ugc6first.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place South American Platinum Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_2nd_sa_platinum",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6secondplatinum",
		models = {
			["basename"] = "models/player/items/all_class/ugc6second.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place South American Platinum Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_3rd_sa_platinum",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6thirdplatinum",
		models = {
			["basename"] = "models/player/items/all_class/ugc6third.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place North American Silver Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_1st_na_silver",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6firstsilver",
		models = {
			["basename"] = "models/player/items/all_class/ugc6first.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place North American Silver Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_2nd_na_silver",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6secondsilver",
		models = {
			["basename"] = "models/player/items/all_class/ugc6second.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place North American Silver Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_3rd_na_silver",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6thirdsilver",
		models = {
			["basename"] = "models/player/items/all_class/ugc6third.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place European Silver Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_1st_europe_silver",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6firstsilver",
		models = {
			["basename"] = "models/player/items/all_class/ugc6first.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place European Silver Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_2nd_europe_silver",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6secondsilver",
		models = {
			["basename"] = "models/player/items/all_class/ugc6second.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place European Silver Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_3rd_europe_silver",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6thirdsilver",
		models = {
			["basename"] = "models/player/items/all_class/ugc6third.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place North American Steel Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_1st_na_steel",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6firststeel",
		models = {
			["basename"] = "models/player/items/all_class/ugc6first.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place North American Steel Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_2nd_na_steel",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6secondsteel",
		models = {
			["basename"] = "models/player/items/all_class/ugc6second.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place North American Steel Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_3rd_na_steel",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6thirdsteel",
		models = {
			["basename"] = "models/player/items/all_class/ugc6third.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place European Steel Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_1st_europe_steel",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6firststeel",
		models = {
			["basename"] = "models/player/items/all_class/ugc6first.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place European Steel Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_2nd_europe_steel",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6secondsteel",
		models = {
			["basename"] = "models/player/items/all_class/ugc6second.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place European Steel Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_3rd_europe_steel",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6thirdsteel",
		models = {
			["basename"] = "models/player/items/all_class/ugc6third.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place South American Steel Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_1st_sa_steel",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6firststeel",
		models = {
			["basename"] = "models/player/items/all_class/ugc6first.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place South American Steel Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_2nd_sa_steel",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6secondsteel",
		models = {
			["basename"] = "models/player/items/all_class/ugc6second.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place South American Steel Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_3rd_sa_steel",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6thirdsteel",
		models = {
			["basename"] = "models/player/items/all_class/ugc6third.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander Platinum Participant Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_highlander_platinum",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6participantplatinum",
		models = {
			["basename"] = "models/player/items/all_class/ugc6participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander Silver Participant Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_highlander_silver",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6participantsilver",
		models = {
			["basename"] = "models/player/items/all_class/ugc6participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander Steel Participant Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc_highlander_steel",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/player/items/all_class/ugc6participantsteel",
		models = {
			["basename"] = "models/player/items/all_class/ugc6participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VI Premier Division 1st Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvi_premier_1st",
		icon = "backpack/player/items/all_class/all_class_badge_esl_gold",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VI Premier Division 2nd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvi_premier_2nd",
		icon = "backpack/player/items/all_class/all_class_badge_esl_silver",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VI Premier Division 3rd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvi_premier_3rd",
		icon = "backpack/player/items/all_class/all_class_badge_esl_bronze",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VI Premier Division Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvi_premier_participant",
		icon = "backpack/player/items/all_class/all_class_badge_esl",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VI Division 1 1st Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvi_div1_1st",
		icon = "backpack/player/items/all_class/all_class_badge_esl_gold",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VI Division 1 2nd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvi_div1_2nd",
		icon = "backpack/player/items/all_class/all_class_badge_esl_silver",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VI Division 1 3rd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvi_div1_3rd",
		icon = "backpack/player/items/all_class/all_class_badge_esl_bronze",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VI Division 1 Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvi_div1_participant",
		icon = "backpack/player/items/all_class/all_class_badge_esl",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VI Division 2 1st Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvi_div2_1st",
		icon = "backpack/player/items/all_class/all_class_badge_esl_gold",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VI Division 2 2nd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvi_div2_2nd",
		icon = "backpack/player/items/all_class/all_class_badge_esl_silver",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VI Division 2 3rd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvi_div2_3rd",
		icon = "backpack/player/items/all_class/all_class_badge_esl_bronze",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VI Division 2 Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvi_div2_participant",
		icon = "backpack/player/items/all_class/all_class_badge_esl",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VI Division 3 1st Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvi_div3_1st",
		icon = "backpack/player/items/all_class/all_class_badge_esl_gold",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VI Division 3 2nd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvi_div3_2nd",
		icon = "backpack/player/items/all_class/all_class_badge_esl_silver",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VI Division 3 3rd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvi_div3_3rd",
		icon = "backpack/player/items/all_class/all_class_badge_esl_bronze",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VI Division 3 Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvi_div3_participant",
		icon = "backpack/player/items/all_class/all_class_badge_esl",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VI Division 4 1st Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvi_div4_1st",
		icon = "backpack/player/items/all_class/all_class_badge_esl_gold",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VI Division 4 2nd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvi_div4_2nd",
		icon = "backpack/player/items/all_class/all_class_badge_esl_silver",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VI Division 4 3rd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvi_div4_3rd",
		icon = "backpack/player/items/all_class/all_class_badge_esl_bronze",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VI Division 4 Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvi_div4_participant",
		icon = "backpack/player/items/all_class/all_class_badge_esl",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VI Division 5 1st Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvi_div5_1st",
		icon = "backpack/player/items/all_class/all_class_badge_esl_gold",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VI Division 5 2nd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvi_div5_2nd",
		icon = "backpack/player/items/all_class/all_class_badge_esl_silver",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VI Division 5 3rd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvi_div5_3rd",
		icon = "backpack/player/items/all_class/all_class_badge_esl_bronze",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VI Division 5 Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvi_div5_participant",
		icon = "backpack/player/items/all_class/all_class_badge_esl",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VII Premiership Division 1st Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvii_premiership_1st",
		icon = "backpack/player/items/all_class/all_class_badge_esl_gold",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VII Premiership Division 2nd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvii_premiership_2nd",
		icon = "backpack/player/items/all_class/all_class_badge_esl_silver",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VII Premiership Division 3rd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvii_premiership_3rd",
		icon = "backpack/player/items/all_class/all_class_badge_esl_bronze",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VII Premiership Division Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvii_premiership_participant",
		icon = "backpack/player/items/all_class/all_class_badge_esl",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VII Division 1 1st Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvii_div1_1st",
		icon = "backpack/player/items/all_class/all_class_badge_esl_gold",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VII Division 1 2nd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvii_div1_2nd",
		icon = "backpack/player/items/all_class/all_class_badge_esl_silver",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VII Division 1 3rd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvii_div1_3rd",
		icon = "backpack/player/items/all_class/all_class_badge_esl_bronze",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VII Division 2 1st Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvii_div2_1st",
		icon = "backpack/player/items/all_class/all_class_badge_esl_gold",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VII Division 2 2nd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvii_div2_2nd",
		icon = "backpack/player/items/all_class/all_class_badge_esl_silver",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VII Division 2 Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvii_div2_participant",
		icon = "backpack/player/items/all_class/all_class_badge_esl",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VII Division 3 Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvii_div3_participant",
		icon = "backpack/player/items/all_class/all_class_badge_esl",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VII Division 4 1st Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvii_div4_1st",
		icon = "backpack/player/items/all_class/all_class_badge_esl_gold",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VII Division 4 2nd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvii_div4_2nd",
		icon = "backpack/player/items/all_class/all_class_badge_esl_silver",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VII Division 4 3rd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvii_div4_3rd",
		icon = "backpack/player/items/all_class/all_class_badge_esl_bronze",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VII Division 4 Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvii_div4_participant",
		icon = "backpack/player/items/all_class/all_class_badge_esl",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VII Division 5 1st Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvii_div5_1st",
		icon = "backpack/player/items/all_class/all_class_badge_esl_gold",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VII Division 5 2nd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvii_div5_2nd",
		icon = "backpack/player/items/all_class/all_class_badge_esl_silver",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VII Division 5 3rd Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvii_div5_3rd",
		icon = "backpack/player/items/all_class/all_class_badge_esl_bronze",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESL Season VII Division 5 Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_esl_seasonvii_div5_participant",
		icon = "backpack/player/items/all_class/all_class_badge_esl",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_esl.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Ready Steady Pan Participant Season 1",
		localized_name = "info.tf2pm.hat.tournamentmedal_ready_steady_pan_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season1",
		icon = "backpack/player/items/all_class/all_class_badge_pan",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_pan.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Ready Steady Pan Helper Season 1",
		localized_name = "info.tf2pm.hat.tournamentmedal_ready_steady_pan_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_ready_steady_pan_helper_season1",
		icon = "backpack/player/items/all_class/all_class_badge_pan",
		models = {
			["basename"] = "models/player/items/all_class/all_class_badge_pan.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Gold Medal Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_gold",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012_gold",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Silver Medal Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_silver",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012_silver",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Bronze Medal Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_bronze",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Gold Medal Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_gold",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012_gold",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Silver Medal Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_silver",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012_silver",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Bronze Medal Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_bronze",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Gold Medal Season 10",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_gold",
		localized_description = "info.tf2pm.hat.tournamentmedal_season10",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012_gold",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Silver Medal Season 10",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_silver",
		localized_description = "info.tf2pm.hat.tournamentmedal_season10",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012_silver",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Bronze Medal Season 10",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_bronze",
		localized_description = "info.tf2pm.hat.tournamentmedal_season10",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Gold Medal Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_gold",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012_gold",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Silver Medal Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_silver",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012_silver",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Bronze Medal Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_bronze",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Gold Medal Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_gold",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012_gold",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Silver Medal Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_silver",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012_silver",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Bronze Medal Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_bronze",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Gold Medal Season 13",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_gold",
		localized_description = "info.tf2pm.hat.tournamentmedal_season13",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012_gold",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Silver Medal Season 13",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_silver",
		localized_description = "info.tf2pm.hat.tournamentmedal_season13",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012_silver",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Bronze Medal Season 13",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_bronze",
		localized_description = "info.tf2pm.hat.tournamentmedal_season13",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 1 Group Winner Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division1_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 2 Group Winner Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division2_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 3 Group Winner Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division3_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 4 Group Winner Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division4_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 5 Group Winner Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division5_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 6 Group Winner Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division6_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 1 Group Winner Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division1_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 2 Group Winner Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division2_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 3 Group Winner Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division3_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 4 Group Winner Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division4_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 5 Group Winner Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division5_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 6 Group Winner Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division6_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 1 Group Winner Season 10",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division1_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season10",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 2 Group Winner Season 10",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division2_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season10",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 3 Group Winner Season 10",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division3_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season10",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 4 Group Winner Season 10",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division4_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season10",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 5 Group Winner Season 10",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division5_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season10",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 6 Group Winner Season 10",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division6_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season10",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 1 Group Winner Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division1_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 2 Group Winner Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division2_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 3 Group Winner Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division3_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 4 Group Winner Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division4_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 5 Group Winner Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division5_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 6 Group Winner Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division6_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 1 Group Winner Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division1_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 2 Group Winner Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division2_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 3 Group Winner Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division3_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 4 Group Winner Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division4_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 5 Group Winner Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division5_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 6 Group Winner Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division6_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 1 Group Winner Season 13",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division1_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season13",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 2 Group Winner Season 13",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division2_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season13",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 3 Group Winner Season 13",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division3_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season13",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 4 Group Winner Season 13",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division4_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season13",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 5 Group Winner Season 13",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division5_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season13",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 6 Group Winner Season 13",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division6_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season13",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Participation Medal Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 1 Participation Medal Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division1_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 2 Participation Medal Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division2_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 3 Participation Medal Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division3_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 4 Participation Medal Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division4_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 5 Participation Medal Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division5_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 6 Participation Medal Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division6_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Participation Medal Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 1 Participation Medal Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division1_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 2 Participation Medal Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division2_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 3 Participation Medal Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division3_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 4 Participation Medal Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division4_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 5 Participation Medal Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division5_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 6 Participation Medal Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division6_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Participation Medal Season 10",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season10",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 1 Participation Medal Season 10",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division1_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season10",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 2 Participation Medal Season 10",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division2_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season10",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 3 Participation Medal Season 10",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division3_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season10",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 4 Participation Medal Season 10",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division4_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season10",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 5 Participation Medal Season 10",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division5_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season10",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 6 Participation Medal Season 10",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division6_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season10",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Participation Medal Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 1 Participation Medal Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division1_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 2 Participation Medal Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division2_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 3 Participation Medal Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division3_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 4 Participation Medal Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division4_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 5 Participation Medal Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division5_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 6 Participation Medal Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division6_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Participation Medal Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 1 Participation Medal Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division1_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 2 Participation Medal Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division2_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 3 Participation Medal Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division3_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 4 Participation Medal Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division4_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 5 Participation Medal Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division5_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 6 Participation Medal Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division6_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Participation Medal Season 13",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season13",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 1 Participation Medal Season 13",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division1_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season13",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 2 Participation Medal Season 13",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division2_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season13",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 3 Participation Medal Season 13",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division3_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season13",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 4 Participation Medal Season 13",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division4_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season13",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 5 Participation Medal Season 13",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division5_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season13",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 6 Participation Medal Season 13",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division6_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season13",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 1 Gold Medal Season 1",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division1_gold",
		localized_description = "info.tf2pm.hat.tournamentmedal_season1",
		icon = "backpack/player/items/all_class/etf2l_highlander_placement_2012_gold",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 1 Silver Medal Season 1",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division1_silver",
		localized_description = "info.tf2pm.hat.tournamentmedal_season1",
		icon = "backpack/player/items/all_class/etf2l_highlander_placement_2012_silver",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 1 Bronze Medal Season 1",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division1_bronze",
		localized_description = "info.tf2pm.hat.tournamentmedal_season1",
		icon = "backpack/player/items/all_class/etf2l_highlander_placement_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 1 Gold Medal Season 2",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division1_gold",
		localized_description = "info.tf2pm.hat.tournamentmedal_season2",
		icon = "backpack/player/items/all_class/etf2l_highlander_placement_2012_gold",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 1 Silver Medal Season 2",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division1_silver",
		localized_description = "info.tf2pm.hat.tournamentmedal_season2",
		icon = "backpack/player/items/all_class/etf2l_highlander_placement_2012_silver",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 1 Bronze Medal Season 2",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division1_bronze",
		localized_description = "info.tf2pm.hat.tournamentmedal_season2",
		icon = "backpack/player/items/all_class/etf2l_highlander_placement_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Premier Division Gold Medal Season 3",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_premier_division_gold",
		localized_description = "info.tf2pm.hat.tournamentmedal_season3",
		icon = "backpack/player/items/all_class/etf2l_highlander_placement_2012_gold",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Premier Division Silver Medal Season 3",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_premier_division_silver",
		localized_description = "info.tf2pm.hat.tournamentmedal_season3",
		icon = "backpack/player/items/all_class/etf2l_highlander_placement_2012_silver",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Premier Division Bronze Medal Season 3",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_premier_division_bronze",
		localized_description = "info.tf2pm.hat.tournamentmedal_season3",
		icon = "backpack/player/items/all_class/etf2l_highlander_placement_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 2 Group Winner Season 1",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division2_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season1",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 3 Group Winner Season 1",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division3_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season1",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 4 Group Winner Season 1",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division4_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season1",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 5 Group Winner Season 1",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division5_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season1",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 6 Group Winner Season 1",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division6_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season1",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 2 Group Winner Season 2",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division2_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season2",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 3 Group Winner Season 2",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division3_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season2",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 4 Group Winner Season 2",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division4_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season2",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 5 Group Winner Season 2",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division5_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season2",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 6 Group Winner Season 2",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division6_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season2",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 1 Group Winner Season 3",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division1_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season3",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 2 Group Winner Season 3",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division2_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season3",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 3 Group Winner Season 3",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division3_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season3",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 4 Group Winner Season 3",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division4_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season3",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 5 Group Winner Season 3",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division5_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season3",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 6 Group Winner Season 3",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division6_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season3",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 1 Participation Medal Season 1",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division1_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season1",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 2 Participation Medal Season 1",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division2_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season1",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 3 Participation Medal Season 1",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division3_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season1",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 4 Participation Medal Season 1",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division4_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season1",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 5 Participation Medal Season 1",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division5_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season1",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 6 Participation Medal Season 1",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division6_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season1",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 1 Participation Medal Season 2",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division1_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season2",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 2 Participation Medal Season 2",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division2_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season2",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 3 Participation Medal Season 2",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division3_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season2",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 4 Participation Medal Season 2",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division4_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season2",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 5 Participation Medal Season 2",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division5_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season2",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 6 Participation Medal Season 2",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division6_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season2",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Premier Division Participation Medal Season 3",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_premier_division_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season3",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 1 Participation Medal Season 3",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division1_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season3",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 2 Participation Medal Season 3",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division2_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season3",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 3 Participation Medal Season 3",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division3_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season3",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 4 Participation Medal Season 3",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division4_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season3",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 5 Participation Medal Season 3",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division5_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season3",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 6 Participation Medal Season 3",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division6_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season3",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Ultiduo 1 Gold Medal",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_ultiduo1_gold_medal",
		icon = "backpack/player/items/all_class/etf2l_ultiduo_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_ultiduo_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Ultiduo 2 Gold Medal",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_ultiduo2_gold_medal",
		icon = "backpack/player/items/all_class/etf2l_ultiduo_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_ultiduo_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Ultiduo 3 Gold Medal",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_ultiduo3_gold_medal",
		icon = "backpack/player/items/all_class/etf2l_ultiduo_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_ultiduo_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Ultiduo 4 Gold Medal",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_ultiduo4_gold_medal",
		icon = "backpack/player/items/all_class/etf2l_ultiduo_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_ultiduo_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESH Ultiduo 1 Gold Medal",
		localized_name = "info.tf2pm.hat.tournamentmedal_esh_ultiduo1_gold_medal",
		icon = "backpack/player/items/all_class/esh_ultiduo_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_ultiduo_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESH Ultiduo 2 Gold Medal",
		localized_name = "info.tf2pm.hat.tournamentmedal_esh_ultiduo2_gold_medal",
		icon = "backpack/player/items/all_class/esh_ultiduo_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_ultiduo_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESH Ultiduo 3 Gold Medal",
		localized_name = "info.tf2pm.hat.tournamentmedal_esh_ultiduo3_gold_medal",
		icon = "backpack/player/items/all_class/esh_ultiduo_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_ultiduo_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESH Ultiduo 4 Gold Medal",
		localized_name = "info.tf2pm.hat.tournamentmedal_esh_ultiduo4_gold_medal",
		icon = "backpack/player/items/all_class/esh_ultiduo_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_ultiduo_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESH Ultiduo 5 Gold Medal",
		localized_name = "info.tf2pm.hat.tournamentmedal_esh_ultiduo5_gold_medal",
		icon = "backpack/player/items/all_class/esh_ultiduo_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_ultiduo_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESH Ultiduo 6 Gold Medal",
		localized_name = "info.tf2pm.hat.tournamentmedal_esh_ultiduo6_gold_medal",
		icon = "backpack/player/items/all_class/esh_ultiduo_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_ultiduo_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ESH Ultiduo 7 Gold Medal",
		localized_name = "info.tf2pm.hat.tournamentmedal_esh_ultiduo7_gold_medal",
		icon = "backpack/player/items/all_class/esh_ultiduo_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_ultiduo_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Tournament Medal (Armory)",
		description = "tournamentmedal",
		localized_name = "info.tf2pm.hat.tournamentmedal_empty",
		icon = "backpack/player/items/soldier/medal",
		models = {
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place Platinum Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_platinum_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/ugc8_medal_first_plat",
		models = {
			["basename"] = "models/player/items/all_class/ugc8_medal_first.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place Platinum Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_platinum_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/ugc8_medal_second_plat",
		models = {
			["basename"] = "models/player/items/all_class/ugc8_medal_second.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place Platinum Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_platinum_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/ugc8_medal_third_plat",
		models = {
			["basename"] = "models/player/items/all_class/ugc8_medal_third.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place Silver Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_silver_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/ugc8_medal_first_silver",
		models = {
			["basename"] = "models/player/items/all_class/ugc8_medal_first.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place Silver Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_silver_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/ugc8_medal_second_silver",
		models = {
			["basename"] = "models/player/items/all_class/ugc8_medal_second.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place Silver Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_silver_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/ugc8_medal_third_silver",
		models = {
			["basename"] = "models/player/items/all_class/ugc8_medal_third.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place Steel Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_steel_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/ugc8_medal_first_steel",
		models = {
			["basename"] = "models/player/items/all_class/ugc8_medal_first.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place Steel Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_steel_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/ugc8_medal_second_steel",
		models = {
			["basename"] = "models/player/items/all_class/ugc8_medal_second.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place Steel Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_steel_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/ugc8_medal_third_steel",
		models = {
			["basename"] = "models/player/items/all_class/ugc8_medal_third.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place Iron Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_iron_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/ugc8_medal_first_iron",
		models = {
			["basename"] = "models/player/items/all_class/ugc8_medal_first.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place Iron Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_iron_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/ugc8_medal_second_iron",
		models = {
			["basename"] = "models/player/items/all_class/ugc8_medal_second.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place Iron Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_iron_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/ugc8_medal_third_iron",
		models = {
			["basename"] = "models/player/items/all_class/ugc8_medal_third.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/ugc8_medal_first_plat",
		models = {
			["basename"] = "models/player/items/all_class/ugc8_medal_first.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/ugc8_medal_second_plat",
		models = {
			["basename"] = "models/player/items/all_class/ugc8_medal_second.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/ugc8_medal_third_plat",
		models = {
			["basename"] = "models/player/items/all_class/ugc8_medal_third.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander Platinum Participant Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_platinum_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/ugc8_medal_participant_plat",
		models = {
			["basename"] = "models/player/items/all_class/ugc8_medal_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander Silver Participant Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_silver_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/ugc8_medal_participant_silver",
		models = {
			["basename"] = "models/player/items/all_class/ugc8_medal_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander Steel Participant Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_steel_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/ugc8_medal_participant_steel",
		models = {
			["basename"] = "models/player/items/all_class/ugc8_medal_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander Participant Season 8",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season8",
		icon = "backpack/player/items/all_class/ugc8_medal_participant_iron",
		models = {
			["basename"] = "models/player/items/all_class/ugc8_medal_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Ready Steady Pan First Place Season 2",
		localized_name = "info.tf2pm.hat.tournamentmedal_ready_steady_pan_first_place",
		localized_description = "info.tf2pm.hat.tournamentmedal_season2",
		icon = "backpack/player/items/all_class/rsp_s02",
		models = {
			["basename"] = "models/player/items/all_class/rsp_s02.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Ready Steady Pan Second Place Season 2",
		localized_name = "info.tf2pm.hat.tournamentmedal_ready_steady_pan_second_place",
		localized_description = "info.tf2pm.hat.tournamentmedal_season2",
		icon = "backpack/player/items/all_class/rsp_s02",
		models = {
			["basename"] = "models/player/items/all_class/rsp_s02.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Ready Steady Pan Third Place Season 2",
		localized_name = "info.tf2pm.hat.tournamentmedal_ready_steady_pan_third_place",
		localized_description = "info.tf2pm.hat.tournamentmedal_season2",
		icon = "backpack/player/items/all_class/rsp_s02",
		models = {
			["basename"] = "models/player/items/all_class/rsp_s02.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Ready Steady Pan Participant Season 2",
		localized_name = "info.tf2pm.hat.tournamentmedal_ready_steady_pan_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season2",
		icon = "backpack/player/items/all_class/rsp_s02",
		models = {
			["basename"] = "models/player/items/all_class/rsp_s02.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Ready Steady Pan Helper Season 2",
		localized_name = "info.tf2pm.hat.tournamentmedal_ready_steady_pan_helper",
		localized_description = "info.tf2pm.hat.tournamentmedal_ready_steady_pan_helper_season2",
		icon = "backpack/player/items/all_class/rsp_s02",
		models = {
			["basename"] = "models/player/items/all_class/rsp_s02.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place Platinum Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_platinum_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/ugc_season9_first_platinum",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_first.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place Platinum Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_platinum_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/ugc_season9_second_platinum",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_second.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place Platinum Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_platinum_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/ugc_season9_third_platinum",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_third.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place Gold Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_gold_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/ugc_season9_first_gold",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_first.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place Gold Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_gold_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/ugc_season9_second_gold",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_second.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place Gold Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_gold_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/ugc_season9_third_gold",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_third.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place Silver Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_silver_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/ugc_season9_first_silver",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_first.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place Silver Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_silver_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/ugc_season9_second_silver",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_second.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place Silver Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_silver_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/ugc_season9_third_silver",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_third.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place Steel Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_steel_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/ugc_season9_first_steel",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_first.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place Steel Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_steel_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/ugc_season9_second_steel",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_second.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place Steel Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_steel_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/ugc_season9_third_steel",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_third.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place Iron Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_iron_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/ugc_season9_first_iron",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_first.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place Iron Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_iron_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/ugc_season9_second_iron",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_second.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place Iron Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_iron_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/ugc_season9_third_iron",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_third.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/ugc_season9_first_platinum",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_first.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/ugc_season9_second_platinum",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_second.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/ugc_season9_third_platinum",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_third.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander Platinum Participant Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_platinum_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/ugc_season9_participation_platinum",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander Gold Participant Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_gold_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/ugc_season9_participation_gold",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander Silver Participant Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_silver_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/ugc_season9_participation_silver",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander Steel Participant Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_steel_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/ugc_season9_participation_steel",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander Iron Participant Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_iron_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/player/items/all_class/ugc_season9_participation_iron",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC 6vs6 Platinum Participant Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc6v6_platinum_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/player/items/all_class/ugc_season9_participation_gold",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC 6vs6 Silver Participant Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc6v6_silver_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/player/items/all_class/ugc_season9_participation_silver",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC 6vs6 Steel Participant Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc6v6_steel_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/player/items/all_class/ugc_season9_participation_steel",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC 6vs6 European Participant Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc6v6_european_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/player/items/all_class/ugc_season9_participation_silver",
		models = {
			["basename"] = "models/player/items/all_class/ugc_season9_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Gold Medal Season 14",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_gold",
		localized_description = "info.tf2pm.hat.tournamentmedal_season14",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012_gold",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Silver Medal Season 14",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_silver",
		localized_description = "info.tf2pm.hat.tournamentmedal_season14",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012_silver",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Bronze Medal Season 14",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_bronze",
		localized_description = "info.tf2pm.hat.tournamentmedal_season14",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 1 Group Winner Season 14",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division1_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season14",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 2 Group Winner Season 14",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division2_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season14",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 3 Group Winner Season 14",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division3_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season14",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 4 Group Winner Season 14",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division4_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season14",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 5 Group Winner Season 14",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division5_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season14",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 6 Group Winner Season 14",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division6_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season14",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Participation Medal Season 14",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season14",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 1 Participation Medal Season 14",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division1_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season14",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 2 Participation Medal Season 14",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division2_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season14",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 3 Participation Medal Season 14",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division3_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season14",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 4 Participation Medal Season 14",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division4_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season14",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 5 Participation Medal Season 14",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division5_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season14",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 6 Participation Medal Season 14",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division6_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season14",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Premier Division Gold Medal Season 4",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_premier_division_gold",
		localized_description = "info.tf2pm.hat.tournamentmedal_season4",
		icon = "backpack/player/items/all_class/etf2l_highlander_placement_2012_gold",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Premier Division Silver Medal Season 4",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_premier_division_silver",
		localized_description = "info.tf2pm.hat.tournamentmedal_season4",
		icon = "backpack/player/items/all_class/etf2l_highlander_placement_2012_silver",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Premier Division Bronze Medal Season 4",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_premier_division_bronze",
		localized_description = "info.tf2pm.hat.tournamentmedal_season4",
		icon = "backpack/player/items/all_class/etf2l_highlander_placement_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 1 Group Winner Season 4",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division1_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season4",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 2 Group Winner Season 4",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division2_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season4",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 3 Group Winner Season 4",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division3_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season4",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 4 Group Winner Season 4",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division4_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season4",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 5 Group Winner Season 4",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division5_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season4",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 6 Group Winner Season 4",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division6_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season4",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Premier Division Participation Medal Season 4",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_premier_division_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season4",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 1 Participation Medal Season 4",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division1_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season4",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 2 Participation Medal Season 4",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division2_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season4",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 3 Participation Medal Season 4",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division3_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season4",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 4 Participation Medal Season 4",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division4_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season4",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 5 Participation Medal Season 4",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division5_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season4",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 6 Participation Medal Season 4",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division6_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season4",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Gold Medal Season 15",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_gold",
		localized_description = "info.tf2pm.hat.tournamentmedal_season15",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012_gold",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Silver Medal Season 15",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_silver",
		localized_description = "info.tf2pm.hat.tournamentmedal_season15",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012_silver",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Bronze Medal Season 15",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_bronze",
		localized_description = "info.tf2pm.hat.tournamentmedal_season15",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 1 Group Winner Season 15",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division1_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season15",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 2 Group Winner Season 15",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division2_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season15",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 3 Group Winner Season 15",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division3_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season15",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 4 Group Winner Season 15",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division4_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season15",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 5 Group Winner Season 15",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division5_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season15",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 6 Group Winner Season 15",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division6_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season15",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Participation Medal Season 15",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season15",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 1 Participation Medal Season 15",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division1_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season15",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 2 Participation Medal Season 15",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division2_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season15",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 3 Participation Medal Season 15",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division3_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season15",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 4 Participation Medal Season 15",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division4_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season15",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 5 Participation Medal Season 15",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division5_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season15",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 6 Participation Medal Season 15",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division6_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season15",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Gold Medal Season 16",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_gold",
		localized_description = "info.tf2pm.hat.tournamentmedal_season16",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012_gold",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Silver Medal Season 16",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_silver",
		localized_description = "info.tf2pm.hat.tournamentmedal_season16",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012_silver",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Bronze Medal Season 16",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_bronze",
		localized_description = "info.tf2pm.hat.tournamentmedal_season16",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 1 Group Winner Season 16",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division1_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season16",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 2 Group Winner Season 16",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division2_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season16",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 3 Group Winner Season 16",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division3_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season16",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 4 Group Winner Season 16",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division4_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season16",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 5 Group Winner Season 16",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division5_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season16",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 6 Group Winner Season 16",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division6_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season16",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Participation Medal Season 16",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season16",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 1 Participation Medal Season 16",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division1_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season16",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 2 Participation Medal Season 16",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division2_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season16",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 3 Participation Medal Season 16",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division3_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season16",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 4 Participation Medal Season 16",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division4_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season16",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 5 Participation Medal Season 16",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division5_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season16",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 6 Participation Medal Season 16",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division6_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season16",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 10 Premier Division First Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl10_6v6_premier_division_first_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_prem_first/ozfortress_sixes_prem_first",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_prem_first/ozfortress_sixes_prem_first_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 10 Premier Division Second Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl10_6v6_premier_division_second_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_prem_second/ozfortress_sixes_prem_second",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_prem_second/ozfortress_sixes_prem_second_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 10 Premier Division Third Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl10_6v6_premier_division_third_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_prem_third/ozfortress_sixes_prem_third",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_prem_third/ozfortress_sixes_prem_third_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 10 Premier Division Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl10_6v6_premier_division_participant",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_prem_participant/ozfortress_sixes_prem_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_prem_participant/ozfortress_sixes_prem_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 10 Division 2 First Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl10_6v6_division2_first_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_first/ozfortress_sixes_regular_first",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_first/ozfortress_sixes_regular_first_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 10 Division 2 Second Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl10_6v6_division2_second_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_second/ozfortress_sixes_regular_second",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_second/ozfortress_sixes_regular_second_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 10 Division 2 Third Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl10_6v6_division2_third_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_third/ozfortress_sixes_regular_third",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_third/ozfortress_sixes_regular_third_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 10 Division 2 Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl10_6v6_division2_participant",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_participant/ozfortress_sixes_regular_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_participant/ozfortress_sixes_regular_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 10 Division 3 First Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl10_6v6_division3_first_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_first/ozfortress_sixes_regular_first",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_first/ozfortress_sixes_regular_first_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 10 Division 3 Second Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl10_6v6_division3_second_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_second/ozfortress_sixes_regular_second",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_second/ozfortress_sixes_regular_second_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 10 Division 3 Third Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl10_6v6_division3_third_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_third/ozfortress_sixes_regular_third",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_third/ozfortress_sixes_regular_third_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 10 Division 3 Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl10_6v6_division3_participant",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_participant/ozfortress_sixes_regular_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_participant/ozfortress_sixes_regular_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 10 Division 4 First Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl10_6v6_division4_first_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_first/ozfortress_sixes_regular_first",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_first/ozfortress_sixes_regular_first_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 10 Division 4 Second Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl10_6v6_division4_second_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_second/ozfortress_sixes_regular_second",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_second/ozfortress_sixes_regular_second_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 10 Division 4 Third Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl10_6v6_division4_third_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_third/ozfortress_sixes_regular_third",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_third/ozfortress_sixes_regular_third_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 10 Division 4 Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl10_6v6_division4_participant",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_participant/ozfortress_sixes_regular_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_participant/ozfortress_sixes_regular_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 10 Division 5 First Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl10_6v6_division5_first_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_first/ozfortress_sixes_regular_first",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_first/ozfortress_sixes_regular_first_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 10 Division 5 Second Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl10_6v6_division5_second_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_second/ozfortress_sixes_regular_second",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_second/ozfortress_sixes_regular_second_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 10 Division 5 Third Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl10_6v6_division5_third_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_third/ozfortress_sixes_regular_third",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_third/ozfortress_sixes_regular_third_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 10 Division 5 Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl10_6v6_division5_participant",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_participant/ozfortress_sixes_regular_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_participant/ozfortress_sixes_regular_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 10 Division 6 First Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl10_6v6_division6_first_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_first/ozfortress_sixes_regular_first",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_first/ozfortress_sixes_regular_first_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 10 Division 6 Second Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl10_6v6_division6_second_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_second/ozfortress_sixes_regular_second",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_second/ozfortress_sixes_regular_second_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 10 Division 6 Third Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl10_6v6_division6_third_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_third/ozfortress_sixes_regular_third",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_third/ozfortress_sixes_regular_third_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 10 Division 6 Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl10_6v6_division6_participant",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_participant/ozfortress_sixes_regular_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_participant/ozfortress_sixes_regular_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "AU Highlander Community League First Place Season 1",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_highlander_community_first_place",
		localized_description = "info.tf2pm.hat.tournamentmedal_season1",
		icon = "backpack/workshop/player/items/all_class/ozfortress_highlander_first/ozfortress_highlander_first",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_highlander_first/ozfortress_highlander_first_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "AU Highlander Community League Second Place Season 1",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_highlander_community_second_place",
		localized_description = "info.tf2pm.hat.tournamentmedal_season1",
		icon = "backpack/workshop/player/items/all_class/ozfortress_highlander_second/ozfortress_highlander_second",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_highlander_second/ozfortress_highlander_second_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "AU Highlander Community League Third Place Season 1",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_highlander_community_third_place",
		localized_description = "info.tf2pm.hat.tournamentmedal_season1",
		icon = "backpack/workshop/player/items/all_class/ozfortress_highlander_third/ozfortress_highlander_third",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_highlander_third/ozfortress_highlander_third_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "AU Highlander Community League Participant Season 1",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_highlander_community_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season1",
		icon = "backpack/workshop/player/items/all_class/ozfortress_highlander_participant/ozfortress_highlander_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_highlander_participant/ozfortress_highlander_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = true,
		name = "Heart of Gold",
		localized_name = "info.tf2pm.hat.medal_heartofgold",
		localized_description = "info.tf2pm.hat.medal_heartofgold_desc",
		icon = "backpack/workshop/player/items/all_class/heart_of_gold/heart_of_gold",
		models = {
			["basename"] = "models/workshop/player/items/all_class/heart_of_gold/heart_of_gold_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = true,
		name = "UGC Highlander 1st Place Platinum Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_platinum_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_platinum_first/ugc_season11_platinum_first",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_platinum_first/ugc_season11_platinum_first_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = true,
		name = "UGC Highlander 2nd Place Platinum Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_platinum_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_platinum_second/ugc_season11_platinum_second",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_platinum_second/ugc_season11_platinum_second_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = true,
		name = "UGC Highlander 3rd Place Platinum Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_platinum_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_platinum_third/ugc_season11_platinum_third",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_platinum_third/ugc_season11_platinum_third_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = true,
		name = "UGC Highlander Platinum Participant Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_platinum_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_platinum_participant/ugc_season11_platinum_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_platinum_participant/ugc_season11_platinum_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place Gold Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_gold_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_gold_first/ugc_season11_gold_first",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_gold_first/ugc_season11_gold_first_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place Gold Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_gold_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_gold_second/ugc_season11_gold_second",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_gold_second/ugc_season11_gold_second_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place Gold Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_gold_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_gold_third/ugc_season11_gold_third",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_gold_third/ugc_season11_gold_third_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander Gold Participant Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_gold_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_gold_participant/ugc_season11_gold_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_gold_participant/ugc_season11_gold_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place Silver Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_silver_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_silver_first/ugc_season11_silver_first",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_silver_first/ugc_season11_silver_first_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place Silver Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_silver_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_silver_second/ugc_season11_silver_second",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_silver_second/ugc_season11_silver_second_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place Silver Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_silver_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_silver_third/ugc_season11_silver_third",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_silver_third/ugc_season11_silver_third_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander Silver Participant Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_silver_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_silver_participant/ugc_season11_silver_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_silver_participant/ugc_season11_silver_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place Steel Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_steel_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_steel_first/ugc_season11_steel_first",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_steel_first/ugc_season11_steel_first_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place Steel Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_steel_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_steel_second/ugc_season11_steel_second",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_steel_second/ugc_season11_steel_second_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place Steel Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_steel_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_steel_third/ugc_season11_steel_third",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_steel_third/ugc_season11_steel_third_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander Steel Participant Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_steel_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_steel_participant/ugc_season11_steel_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_steel_participant/ugc_season11_steel_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place Iron Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_iron_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_iron_first/ugc_season11_iron_first",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_iron_first/ugc_season11_iron_first_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place Iron Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_iron_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_iron_second/ugc_season11_iron_second",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_iron_second/ugc_season11_iron_second_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place Iron Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_iron_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_iron_third/ugc_season11_iron_third",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_iron_third/ugc_season11_iron_third_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander Iron Participant Season 11",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_iron_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season11",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_iron_participant/ugc_season11_iron_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_iron_participant/ugc_season11_iron_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = true,
		name = "UGC 6vs6 1st Place Platinum Season 13",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc6v6_platinum_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season13",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_platinum_first/ugc_season11_platinum_first",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_platinum_first/ugc_season11_platinum_first_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = true,
		name = "UGC 6vs6 2nd Place Platinum Season 13",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc6v6_platinum_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season13",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_platinum_second/ugc_season11_platinum_second",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_platinum_second/ugc_season11_platinum_second_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = true,
		name = "UGC 6vs6 3rd Place Platinum Season 13",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc6v6_platinum_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season13",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_platinum_third/ugc_season11_platinum_third",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_platinum_third/ugc_season11_platinum_third_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = true,
		name = "UGC 6vs6 Platinum Participant Season 13",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc6v6_platinum_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season13",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_platinum_participant/ugc_season11_platinum_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_platinum_participant/ugc_season11_platinum_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC 6vs6 Silver Participant Season 13",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc6v6_silver_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season13",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_silver_participant/ugc_season11_silver_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_silver_participant/ugc_season11_silver_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC 6vs6 Steel Participant Season 13",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc6v6_steel_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season13",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_steel_participant/ugc_season11_steel_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_steel_participant/ugc_season11_steel_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC 6vs6 Iron Participant Season 13",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc6v6_iron_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season13",
		icon = "backpack/workshop/player/items/all_class/ugc_season11_iron_participant/ugc_season11_iron_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season11_iron_participant/ugc_season11_iron_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "Tumblr Vs Reddit Participant Season 1",
		localized_name = "info.tf2pm.hat.tournamentmedal_tumblrvsreddit",
		localized_description = "info.tf2pm.hat.tournamentmedal_tumblrvsreddit_season1",
		icon = "backpack/workshop/player/items/all_class/tvr_medal/tvr_medal",
		models = {
			["basename"] = "models/workshop/player/items/all_class/tvr_medal/tvr_medal_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "InfoShow LAN Party 2014 Participant",
		localized_name = "info.tf2pm.hat.medal_infoshow2014",
		localized_description = "info.tf2pm.hat.medal_infoshow2014_desc",
		icon = "backpack/workshop/player/items/all_class/infoshowmedal14/infoshowmedal14",
		models = {
			["basename"] = "models/workshop/player/items/all_class/infoshowmedal14/infoshowmedal14.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Gold Medal Season 17",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_gold",
		localized_description = "info.tf2pm.hat.tournamentmedal_season17",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012_gold",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Silver Medal Season 17",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_silver",
		localized_description = "info.tf2pm.hat.tournamentmedal_season17",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012_silver",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Bronze Medal Season 17",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_bronze",
		localized_description = "info.tf2pm.hat.tournamentmedal_season17",
		icon = "backpack/player/items/all_class/etf2l_sixs_placement_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 1 Group Winner Season 17",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division1_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season17",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 2 Group Winner Season 17",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division2_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season17",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 3 Group Winner Season 17",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division3_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season17",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 4 Group Winner Season 17",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division4_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season17",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 5 Group Winner Season 17",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division5_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season17",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 6 Group Winner Season 17",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division6_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season17",
		icon = "backpack/player/items/all_class/etf2l_sixs_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Participation Medal Season 17",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season17",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 1 Participation Medal Season 17",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division1_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season17",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 2 Participation Medal Season 17",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division2_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season17",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 3 Participation Medal Season 17",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division3_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season17",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 4 Participation Medal Season 17",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division4_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season17",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 5 Participation Medal Season 17",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division5_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season17",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 6 Participation Medal Season 17",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division6_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season17",
		icon = "backpack/player/items/all_class/etf2l_sixs_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_sixs_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Premier Division Gold Medal Season 5",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_premier_division_gold",
		localized_description = "info.tf2pm.hat.tournamentmedal_season5",
		icon = "backpack/player/items/all_class/etf2l_highlander_placement_2012_gold",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Premier Division Silver Medal Season 5",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_premier_division_silver",
		localized_description = "info.tf2pm.hat.tournamentmedal_season5",
		icon = "backpack/player/items/all_class/etf2l_highlander_placement_2012_silver",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Premier Division Bronze Medal Season 5",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_premier_division_bronze",
		localized_description = "info.tf2pm.hat.tournamentmedal_season5",
		icon = "backpack/player/items/all_class/etf2l_highlander_placement_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_placement_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 1 Group Winner Season 5",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division1_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season5",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 2 Group Winner Season 5",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division2_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season5",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 3 Group Winner Season 5",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division3_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season5",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 4 Group Winner Season 5",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division4_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season5",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 5 Group Winner Season 5",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division5_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season5",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 6 Group Winner Season 5",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division6_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season5",
		icon = "backpack/player/items/all_class/etf2l_highlander_winner_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_winner_2012.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Premier Division Participation Medal Season 5",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_premier_division_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season5",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 1 Participation Medal Season 5",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division1_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season5",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 2 Participation Medal Season 5",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division2_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season5",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 3 Participation Medal Season 5",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division3_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season5",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 4 Participation Medal Season 5",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division4_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season5",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 5 Participation Medal Season 5",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division5_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season5",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Division 6 Participation Medal Season 5",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_division6_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season5",
		icon = "backpack/player/items/all_class/etf2l_highlander_participant_2012",
		models = {
			["basename"] = "models/player/items/all_class/etf2l_highlander_participant_2012.mdl",
		},
		styles = {
			[0] = {
				skin = 0,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style0",
			},
			[1] = {
				skin = 1,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style1",
			},
			[2] = {
				skin = 2,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style2",
			},
			[3] = {
				skin = 3,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style3",
			},
			[4] = {
				skin = 4,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style4",
			},
			[5] = {
				skin = 5,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style5",
			},
			[6] = {
				skin = 6,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style6",
			},
			[7] = {
				skin = 7,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style7",
			},
			[8] = {
				skin = 8,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style8",
			},
			[9] = {
				skin = 9,
				localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_participation_style9",
			},
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "LBTF2 6v6 Elite 1st Place Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_lbtf2_6v6_elite_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/workshop/player/items/all_class/lbtf_medal_gold/lbtf_medal_gold",
		models = {
			["basename"] = "models/workshop/player/items/all_class/lbtf_medal_gold/lbtf_medal_gold_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "LBTF2 6v6 Elite 2nd Place Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_lbtf2_6v6_elite_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/workshop/player/items/all_class/lbtf_medal_silver/lbtf_medal_silver",
		models = {
			["basename"] = "models/workshop/player/items/all_class/lbtf_medal_silver/lbtf_medal_silver_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "LBTF2 6v6 Elite 3rd Place Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_lbtf2_6v6_elite_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/workshop/player/items/all_class/lbtf_medal_bronze/lbtf_medal_bronze",
		models = {
			["basename"] = "models/workshop/player/items/all_class/lbtf_medal_bronze/lbtf_medal_bronze_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "LBTF2 6v6 Elite Participant Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_lbtf2_6v6_elite_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/workshop/player/items/all_class/lbtf_medal_participant/lbtf_medal_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/lbtf_medal_participant/lbtf_medal_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "LBTF2 6v6 Central 1st Place Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_lbtf2_6v6_central_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/workshop/player/items/all_class/lbtf_medal_gold/lbtf_medal_gold",
		models = {
			["basename"] = "models/workshop/player/items/all_class/lbtf_medal_gold/lbtf_medal_gold_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "LBTF2 6v6 Central 2nd Place Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_lbtf2_6v6_central_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/workshop/player/items/all_class/lbtf_medal_silver/lbtf_medal_silver",
		models = {
			["basename"] = "models/workshop/player/items/all_class/lbtf_medal_silver/lbtf_medal_silver_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "LBTF2 6v6 Central 3rd Place Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_lbtf2_6v6_central_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/workshop/player/items/all_class/lbtf_medal_bronze/lbtf_medal_bronze",
		models = {
			["basename"] = "models/workshop/player/items/all_class/lbtf_medal_bronze/lbtf_medal_bronze_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "LBTF2 6v6 Central Participant Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_lbtf2_6v6_central_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/workshop/player/items/all_class/lbtf_medal_participant/lbtf_medal_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/lbtf_medal_participant/lbtf_medal_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "LBTF2 6v6 Access 1st Place Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_lbtf2_6v6_access_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/workshop/player/items/all_class/lbtf_medal_gold/lbtf_medal_gold",
		models = {
			["basename"] = "models/workshop/player/items/all_class/lbtf_medal_gold/lbtf_medal_gold_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "LBTF2 6v6 Access 2nd Place Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_lbtf2_6v6_access_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/workshop/player/items/all_class/lbtf_medal_silver/lbtf_medal_silver",
		models = {
			["basename"] = "models/workshop/player/items/all_class/lbtf_medal_silver/lbtf_medal_silver_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "LBTF2 6v6 Access 3rd Place Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_lbtf2_6v6_access_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/workshop/player/items/all_class/lbtf_medal_bronze/lbtf_medal_bronze",
		models = {
			["basename"] = "models/workshop/player/items/all_class/lbtf_medal_bronze/lbtf_medal_bronze_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "LBTF2 6v6 Access Participant Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_lbtf2_6v6_access_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/workshop/player/items/all_class/lbtf_medal_participant/lbtf_medal_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/lbtf_medal_participant/lbtf_medal_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "LBTF2 6v6 Open 1st Place Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_lbtf2_6v6_open_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/workshop/player/items/all_class/lbtf_medal_gold/lbtf_medal_gold",
		models = {
			["basename"] = "models/workshop/player/items/all_class/lbtf_medal_gold/lbtf_medal_gold_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "LBTF2 6v6 Open 2nd Place Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_lbtf2_6v6_open_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/workshop/player/items/all_class/lbtf_medal_silver/lbtf_medal_silver",
		models = {
			["basename"] = "models/workshop/player/items/all_class/lbtf_medal_silver/lbtf_medal_silver_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "LBTF2 6v6 Open 3rd Place Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_lbtf2_6v6_open_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/workshop/player/items/all_class/lbtf_medal_bronze/lbtf_medal_bronze",
		models = {
			["basename"] = "models/workshop/player/items/all_class/lbtf_medal_bronze/lbtf_medal_bronze_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "LBTF2 6v6 Open Participant Season 9",
		localized_name = "info.tf2pm.hat.tournamentmedal_lbtf2_6v6_open_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season9",
		icon = "backpack/workshop/player/items/all_class/lbtf_medal_participant/lbtf_medal_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/lbtf_medal_participant/lbtf_medal_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "AsiaFortress Cup Division 1 1st Place Season 7",
		localized_name = "info.tf2pm.hat.tournamentmedal_afc_div1_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season7",
		icon = "backpack/workshop/player/items/all_class/asiafortress_gold/asiafortress_gold",
		models = {
			["basename"] = "models/workshop/player/items/all_class/asiafortress_gold/asiafortress_gold_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "AsiaFortress Cup Division 1 2nd Place Season 7",
		localized_name = "info.tf2pm.hat.tournamentmedal_afc_div1_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season7",
		icon = "backpack/workshop/player/items/all_class/asiafortress_gold/asiafortress_gold",
		models = {
			["basename"] = "models/workshop/player/items/all_class/asiafortress_gold/asiafortress_gold_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "AsiaFortress Cup Division 1 3rd Place Season 7",
		localized_name = "info.tf2pm.hat.tournamentmedal_afc_div1_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season7",
		icon = "backpack/workshop/player/items/all_class/asiafortress_gold/asiafortress_gold",
		models = {
			["basename"] = "models/workshop/player/items/all_class/asiafortress_gold/asiafortress_gold_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "AsiaFortress Cup Division 1 Participant Season 7",
		localized_name = "info.tf2pm.hat.tournamentmedal_afc_div1_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season7",
		icon = "backpack/workshop/player/items/all_class/asiafortress_participant/asiafortress_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/asiafortress_participant/asiafortress_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "AsiaFortress Cup Division 2 1st Place Season 7",
		localized_name = "info.tf2pm.hat.tournamentmedal_afc_div2_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season7",
		icon = "backpack/workshop/player/items/all_class/asiafortress_silver/asiafortress_silver",
		models = {
			["basename"] = "models/workshop/player/items/all_class/asiafortress_silver/asiafortress_silver_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "AsiaFortress Cup Division 2 2nd Place Season 7",
		localized_name = "info.tf2pm.hat.tournamentmedal_afc_div2_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season7",
		icon = "backpack/workshop/player/items/all_class/asiafortress_silver/asiafortress_silver",
		models = {
			["basename"] = "models/workshop/player/items/all_class/asiafortress_silver/asiafortress_silver_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "AsiaFortress Cup Division 2 3rd Place Season 7",
		localized_name = "info.tf2pm.hat.tournamentmedal_afc_div2_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season7",
		icon = "backpack/workshop/player/items/all_class/asiafortress_silver/asiafortress_silver",
		models = {
			["basename"] = "models/workshop/player/items/all_class/asiafortress_silver/asiafortress_silver_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "AsiaFortress Cup Division 2 Participant Season 7",
		localized_name = "info.tf2pm.hat.tournamentmedal_afc_div2_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season7",
		icon = "backpack/workshop/player/items/all_class/asiafortress_participant/asiafortress_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/asiafortress_participant/asiafortress_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "AsiaFortress Cup Division 3 1st Place Season 7",
		localized_name = "info.tf2pm.hat.tournamentmedal_afc_div3_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season7",
		icon = "backpack/workshop/player/items/all_class/asiafortress_bronze/asiafortress_bronze",
		models = {
			["basename"] = "models/workshop/player/items/all_class/asiafortress_bronze/asiafortress_bronze_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "AsiaFortress Cup Division 3 2nd Place Season 7",
		localized_name = "info.tf2pm.hat.tournamentmedal_afc_div3_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season7",
		icon = "backpack/workshop/player/items/all_class/asiafortress_bronze/asiafortress_bronze",
		models = {
			["basename"] = "models/workshop/player/items/all_class/asiafortress_bronze/asiafortress_bronze_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "AsiaFortress Cup Division 3 3rd Place Season 7",
		localized_name = "info.tf2pm.hat.tournamentmedal_afc_div3_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season7",
		icon = "backpack/workshop/player/items/all_class/asiafortress_bronze/asiafortress_bronze",
		models = {
			["basename"] = "models/workshop/player/items/all_class/asiafortress_bronze/asiafortress_bronze_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "AsiaFortress Cup Division 3 Participant Season 7",
		localized_name = "info.tf2pm.hat.tournamentmedal_afc_div3_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season7",
		icon = "backpack/workshop/player/items/all_class/asiafortress_participant/asiafortress_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/asiafortress_participant/asiafortress_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 11 Premier Division First Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl11_6v6_premier_division_first_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_prem_first/ozfortress_sixes_prem_first",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_prem_first/ozfortress_sixes_prem_first_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 11 Premier Division Second Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl11_6v6_premier_division_second_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_prem_second/ozfortress_sixes_prem_second",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_prem_second/ozfortress_sixes_prem_second_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 11 Premier Division Third Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl11_6v6_premier_division_third_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_prem_third/ozfortress_sixes_prem_third",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_prem_third/ozfortress_sixes_prem_third_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 11 Premier Division Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl11_6v6_premier_division_participant",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_prem_participant/ozfortress_sixes_prem_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_prem_participant/ozfortress_sixes_prem_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 11 Division 2 First Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl11_6v6_division2_first_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_first/ozfortress_sixes_regular_first",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_first/ozfortress_sixes_regular_first_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 11 Division 2 Second Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl11_6v6_division2_second_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_second/ozfortress_sixes_regular_second",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_second/ozfortress_sixes_regular_second_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 11 Division 2 Third Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl11_6v6_division2_third_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_third/ozfortress_sixes_regular_third",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_third/ozfortress_sixes_regular_third_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 11 Division 2 Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl11_6v6_division2_participant",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_participant/ozfortress_sixes_regular_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_participant/ozfortress_sixes_regular_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 11 Division 3 First Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl11_6v6_division3_first_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_first/ozfortress_sixes_regular_first",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_first/ozfortress_sixes_regular_first_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 11 Division 3 Second Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl11_6v6_division3_second_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_second/ozfortress_sixes_regular_second",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_second/ozfortress_sixes_regular_second_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 11 Division 3 Third Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl11_6v6_division3_third_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_third/ozfortress_sixes_regular_third",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_third/ozfortress_sixes_regular_third_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 11 Division 3 Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl11_6v6_division3_participant",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_participant/ozfortress_sixes_regular_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_participant/ozfortress_sixes_regular_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 11 Division 4 First Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl11_6v6_division4_first_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_first/ozfortress_sixes_regular_first",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_first/ozfortress_sixes_regular_first_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 11 Division 4 Second Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl11_6v6_division4_second_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_second/ozfortress_sixes_regular_second",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_second/ozfortress_sixes_regular_second_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 11 Division 4 Third Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl11_6v6_division4_third_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_third/ozfortress_sixes_regular_third",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_third/ozfortress_sixes_regular_third_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 11 Division 4 Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl11_6v6_division4_participant",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_participant/ozfortress_sixes_regular_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_participant/ozfortress_sixes_regular_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 11 Division 5 First Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl11_6v6_division5_first_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_first/ozfortress_sixes_regular_first",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_first/ozfortress_sixes_regular_first_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 11 Division 5 Second Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl11_6v6_division5_second_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_second/ozfortress_sixes_regular_second",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_second/ozfortress_sixes_regular_second_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 11 Division 5 Third Place",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl11_6v6_division5_third_place",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_third/ozfortress_sixes_regular_third",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_third/ozfortress_sixes_regular_third_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "OWL 11 Division 5 Participant",
		localized_name = "info.tf2pm.hat.tournamentmedal_ozfortress_owl11_6v6_division5_participant",
		icon = "backpack/workshop/player/items/all_class/ozfortress_sixes_regular_participant/ozfortress_sixes_regular_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ozfortress_sixes_regular_participant/ozfortress_sixes_regular_participant_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = true,
		name = "Tumblr Vs Reddit Participant Season 2",
		localized_name = "info.tf2pm.hat.tournamentmedal_tumblrvsreddit",
		localized_description = "info.tf2pm.hat.tournamentmedal_tumblrvsreddit_season2",
		icon = "backpack/workshop/player/items/all_class/tvr_medal_season2/tvr_medal_season2",
		models = {
			["basename"] = "models/workshop/player/items/all_class/tvr_medal_season2/tvr_medal_season2_demo.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place Platinum Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_platinum_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/workshop/player/items/all_class/ugc_season12/ugc_season12_platinum_first",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season12/ugc_season12_platinum_first.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place Platinum Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_platinum_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/workshop/player/items/all_class/ugc_season12/ugc_season12_platinum_second",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season12/ugc_season12_platinum_second.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place Platinum Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_platinum_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/workshop/player/items/all_class/ugc_season12/ugc_season12_platinum_third",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season12/ugc_season12_platinum_third.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander Platinum Participant Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_platinum_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/workshop/player/items/all_class/ugc_season12/ugc_season12_platinum_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season12/ugc_season12_platinum_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place Gold Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_gold_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/workshop/player/items/all_class/ugc_season12/ugc_season12_gold_first",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season12/ugc_season12_first.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place Gold Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_gold_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/workshop/player/items/all_class/ugc_season12/ugc_season12_gold_second",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season12/ugc_season12_second.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place Gold Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_gold_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/workshop/player/items/all_class/ugc_season12/ugc_season12_gold_third",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season12/ugc_season12_third.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander Gold Participant Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_gold_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/workshop/player/items/all_class/ugc_season12/ugc_season12_gold_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season12/ugc_season12_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 1st Place Silver Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_silver_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/workshop/player/items/all_class/ugc_season12/ugc_season12_silver_first",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season12/ugc_season12_first.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 2nd Place Silver Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_silver_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/workshop/player/items/all_class/ugc_season12/ugc_season12_silver_second",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season12/ugc_season12_second.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander 3rd Place Silver Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_silver_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/workshop/player/items/all_class/ugc_season12/ugc_season12_silver_third",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season12/ugc_season12_third.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander Silver Participant Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_silver_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/workshop/player/items/all_class/ugc_season12/ugc_season12_silver_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season12/ugc_season12_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander Steel Participant Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_steel_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/workshop/player/items/all_class/ugc_season12/ugc_season12_steel_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season12/ugc_season12_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC Highlander Iron Participant Season 12",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugchl_iron_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season12",
		icon = "backpack/workshop/player/items/all_class/ugc_season12/ugc_season12_iron_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season12/ugc_season12_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC 6vs6 1st Place Platinum Season 14",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc6v6_platinum_1st",
		localized_description = "info.tf2pm.hat.tournamentmedal_season14",
		icon = "backpack/workshop/player/items/all_class/ugc_season12/ugc_season12_platinum_first",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season12/ugc_season12_platinum_first.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC 6vs6 2nd Place Platinum Season 14",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc6v6_platinum_2nd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season14",
		icon = "backpack/workshop/player/items/all_class/ugc_season12/ugc_season12_platinum_second",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season12/ugc_season12_platinum_second.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC 6vs6 3rd Place Platinum Season 14",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc6v6_platinum_3rd",
		localized_description = "info.tf2pm.hat.tournamentmedal_season14",
		icon = "backpack/workshop/player/items/all_class/ugc_season12/ugc_season12_platinum_third",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season12/ugc_season12_platinum_third.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC 6vs6 Platinum Participant Season 14",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc6v6_platinum_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season14",
		icon = "backpack/workshop/player/items/all_class/ugc_season12/ugc_season12_platinum_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season12/ugc_season12_platinum_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC 6vs6 Gold Participant Season 14",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc6v6_gold_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season14",
		icon = "backpack/workshop/player/items/all_class/ugc_season12/ugc_season12_gold_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season12/ugc_season12_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC 6vs6 Silver Participant Season 14",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc6v6_silver_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season14",
		icon = "backpack/workshop/player/items/all_class/ugc_season12/ugc_season12_silver_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season12/ugc_season12_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC 6vs6 Steel Participant Season 14",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc6v6_steel_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season14",
		icon = "backpack/workshop/player/items/all_class/ugc_season12/ugc_season12_steel_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season12/ugc_season12_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC 6vs6 Iron Participant Season 14",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc6v6_iron_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season14",
		icon = "backpack/workshop/player/items/all_class/ugc_season12/ugc_season12_iron_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season12/ugc_season12_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC 4vs4 Silver Participant Season 1",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc4v4_silver_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season1",
		icon = "backpack/workshop/player/items/all_class/ugc_season12/ugc_season12_silver_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season12/ugc_season12_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "UGC 4vs4 Steel Participant Season 1",
		localized_name = "info.tf2pm.hat.tournamentmedal_ugc4v4_steel_participant",
		localized_description = "info.tf2pm.hat.tournamentmedal_season1",
		icon = "backpack/workshop/player/items/all_class/ugc_season12/ugc_season12_steel_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/ugc_season12/ugc_season12_participant.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Gold Medal Season 18",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_gold",
		localized_description = "info.tf2pm.hat.tournamentmedal_season18",
		icon = "backpack/workshop/player/items/all_class/etf2l_2014_6v6_1st_place/etf2l_2014_6v6_1st_place",
		models = {
			["basename"] = "models/workshop/player/items/all_class/etf2l_2014_6v6_1st_place/etf2l_2014_6v6_1st_place_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Silver Medal Season 18",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_silver",
		localized_description = "info.tf2pm.hat.tournamentmedal_season18",
		icon = "backpack/workshop/player/items/all_class/etf2l_2014_6v6_2nd_place/etf2l_2014_6v6_2nd_place",
		models = {
			["basename"] = "models/workshop/player/items/all_class/etf2l_2014_6v6_2nd_place/etf2l_2014_6v6_2nd_place_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Bronze Medal Season 18",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_bronze",
		localized_description = "info.tf2pm.hat.tournamentmedal_season18",
		icon = "backpack/workshop/player/items/all_class/etf2l_2014_6v6_3rd_place/etf2l_2014_6v6_3rd_place",
		models = {
			["basename"] = "models/workshop/player/items/all_class/etf2l_2014_6v6_3rd_place/etf2l_2014_6v6_3rd_place_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 1 Group Winner Season 18",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division1_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season18",
		icon = "backpack/workshop/player/items/all_class/etf2l_2014_6v6_group_winner/etf2l_2014_6v6_group_winner",
		models = {
			["basename"] = "models/workshop/player/items/all_class/etf2l_2014_6v6_group_winner/etf2l_2014_6v6_group_winner_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 2 Group Winner Season 18",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division2_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season18",
		icon = "backpack/workshop/player/items/all_class/etf2l_2014_6v6_group_winner/etf2l_2014_6v6_group_winner",
		models = {
			["basename"] = "models/workshop/player/items/all_class/etf2l_2014_6v6_group_winner/etf2l_2014_6v6_group_winner_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 3 Group Winner Season 18",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division3_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season18",
		icon = "backpack/workshop/player/items/all_class/etf2l_2014_6v6_group_winner/etf2l_2014_6v6_group_winner",
		models = {
			["basename"] = "models/workshop/player/items/all_class/etf2l_2014_6v6_group_winner/etf2l_2014_6v6_group_winner_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 4 Group Winner Season 18",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division4_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season18",
		icon = "backpack/workshop/player/items/all_class/etf2l_2014_6v6_group_winner/etf2l_2014_6v6_group_winner",
		models = {
			["basename"] = "models/workshop/player/items/all_class/etf2l_2014_6v6_group_winner/etf2l_2014_6v6_group_winner_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 5 Group Winner Season 18",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division5_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season18",
		icon = "backpack/workshop/player/items/all_class/etf2l_2014_6v6_group_winner/etf2l_2014_6v6_group_winner",
		models = {
			["basename"] = "models/workshop/player/items/all_class/etf2l_2014_6v6_group_winner/etf2l_2014_6v6_group_winner_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 6 Group Winner Season 18",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division6_group_winner",
		localized_description = "info.tf2pm.hat.tournamentmedal_season18",
		icon = "backpack/workshop/player/items/all_class/etf2l_2014_6v6_group_winner/etf2l_2014_6v6_group_winner",
		models = {
			["basename"] = "models/workshop/player/items/all_class/etf2l_2014_6v6_group_winner/etf2l_2014_6v6_group_winner_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Premier Division Participation Medal Season 18",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_premier_division_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season18",
		icon = "backpack/workshop/player/items/all_class/etf2l_2014_6v6_participant/etf2l_2014_6v6_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/etf2l_2014_6v6_participant/etf2l_2014_6v6_participant_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 1 Participation Medal Season 18",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division1_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season18",
		icon = "backpack/workshop/player/items/all_class/etf2l_2014_6v6_participant/etf2l_2014_6v6_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/etf2l_2014_6v6_participant/etf2l_2014_6v6_participant_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 2 Participation Medal Season 18",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division2_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season18",
		icon = "backpack/workshop/player/items/all_class/etf2l_2014_6v6_participant/etf2l_2014_6v6_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/etf2l_2014_6v6_participant/etf2l_2014_6v6_participant_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 3 Participation Medal Season 18",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division3_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season18",
		icon = "backpack/workshop/player/items/all_class/etf2l_2014_6v6_participant/etf2l_2014_6v6_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/etf2l_2014_6v6_participant/etf2l_2014_6v6_participant_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 4 Participation Medal Season 18",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division4_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season18",
		icon = "backpack/workshop/player/items/all_class/etf2l_2014_6v6_participant/etf2l_2014_6v6_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/etf2l_2014_6v6_participant/etf2l_2014_6v6_participant_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 5 Participation Medal Season 18",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division5_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season18",
		icon = "backpack/workshop/player/items/all_class/etf2l_2014_6v6_participant/etf2l_2014_6v6_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/etf2l_2014_6v6_participant/etf2l_2014_6v6_participant_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L 6v6 Division 6 Participation Medal Season 18",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_6v6_division6_participation",
		localized_description = "info.tf2pm.hat.tournamentmedal_season18",
		icon = "backpack/workshop/player/items/all_class/etf2l_2014_6v6_participant/etf2l_2014_6v6_participant",
		models = {
			["basename"] = "models/workshop/player/items/all_class/etf2l_2014_6v6_participant/etf2l_2014_6v6_participant_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Premier Division Gold Medal Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_premier_division_gold",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/workshop/player/items/all_class/etf2l_2014_highlander_1st_place/etf2l_2014_highlander_1st_place",
		models = {
			["basename"] = "models/workshop/player/items/all_class/etf2l_2014_highlander_1st_place/etf2l_2014_highlander_1st_place_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Premier Division Silver Medal Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_premier_division_silver",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/workshop/player/items/all_class/etf2l_2014_highlander_2nd_place/etf2l_2014_highlander_2nd_place",
		models = {
			["basename"] = "models/workshop/player/items/all_class/etf2l_2014_highlander_2nd_place/etf2l_2014_highlander_2nd_place_%s.mdl",
		},
	},
	{
		classes = {"heavy", "medic", "scout", "pyro", "demo", "soldier", "spy", "engineer", "sniper"},
		targets = {"medal"},
		paintable = false,
		name = "ETF2L Highlander Premier Division Bronze Medal Season 6",
		localized_name = "info.tf2pm.hat.tournamentmedal_etf2l_highlander_premier_division_bronze",
		localized_description = "info.tf2pm.hat.tournamentmedal_season6",
		icon = "backpack/workshop/player/items/all_class/etf2l_2014_highlander_3rd_place/etf2l_2014_highlander_3rd_place",
		models = {
			["basename"] = "models/workshop/player/items/all_class/etf2l_2014_highlander_3rd_place/etf2l_2014_highlander_3rd_place_%s.mdl",
		},
	},

}
