
-- Copyright (c) 2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.

local tf2_pm = tf2_pm
local net = DLib.net

net.receive('tf2_pm_apply_hat', function()
	local ent = net.ReadEntity()
	if not IsValid(ent) then return end
	local hat = net.ReadString()

	local pm = ent:GetTF2PM()
	pm:WearHat(hat)
end)

net.receive('tf2_pm_remove_hat', function()
	local ent = net.ReadEntity()
	if not IsValid(ent) then return end
	local hat = net.ReadString()

	local pm = ent:GetTF2PM()
	pm:RemoveHat(hat)
end)

net.receive('tf2_pm_set_hat_style', function()
	local ent = net.ReadEntity()
	if not IsValid(ent) then return end
	local hat = net.ReadString()
	local style = net.ReadUInt16()

	local pm = ent:GetTF2PM()
	pm:SetHatStyle(hat, style)
end)

net.receive('tf2_pm_unset_hat_style', function()
	local ent = net.ReadEntity()
	if not IsValid(ent) then return end
	local hat = net.ReadString()

	local pm = ent:GetTF2PM()
	pm:UnsetHatStyle(hat)
end)

net.receive('tf2_pm_set_hat_color', function()
	local ent = net.ReadEntity()
	if not IsValid(ent) then return end
	local hat = net.ReadString()
	local color = net.ReadColor()

	local pm = ent:GetTF2PM()
	pm:SetHatColor(hat, color)
end)

net.receive('tf2_pm_unset_hat_color', function()
	local ent = net.ReadEntity()
	if not IsValid(ent) then return end
	local hat = net.ReadString()

	local pm = ent:GetTF2PM()
	pm:UnsetHatColor(hat)
end)

net.receive('tf2_pm_set_team', function()
	local ent = net.ReadEntity()
	if not IsValid(ent) then return end

	local pm = ent:GetTF2PM()
	pm:SetTeam(net.ReadUInt16())
end)

net.receive('tf2_pm_unset_team', function()
	local ent = net.ReadEntity()
	if not IsValid(ent) then return end

	local pm = ent:GetTF2PM()
	pm:UnsetTeam()
end)

net.receive('tf2_pm_player_model_update', function()
	local ent = net.ReadEntity()
	if not IsValid(ent) then return end
	if not ent.tf2_pm then return end

	ent.tf2_pm:RebuildModelDef()
	ent.tf2_pm:UpdateModels()

	ent.tf2_pm:ProcessBodygroups()
	ent.tf2_pm:ProcessSkins()

	timer.Simple(0.25, function()
		if not IsValid(ent) then return end
		ent.tf2_pm:ProcessBodygroups()
		ent.tf2_pm:ProcessSkins()
	end)
end)

net.receive('tf2_pm_set_bodygroup', function()
	local ent = net.ReadEntity()
	if not IsValid(ent) then return end
	if not ent.tf2_pm then return end

	if net.ReadBool() then
		ent.tf2_pm:SetCustomBodygroup(net.ReadString(), net.ReadUInt8())
	else
		ent.tf2_pm:UnsetCustomBodygroup(net.ReadString())
	end
end)

function tf2_pm.RequestRemoveAllBodygroups(bodygroup, value)
	net.Start('tf2_pm_request_unbodygroup')
	net.SendToServer()
end

function tf2_pm.RequestSetBodygroup(bodygroup, value)
	net.Start('tf2_pm_request_bodygroup')
	net.WriteBool(true)
	net.WriteString(bodygroup)
	net.WriteUInt8(value)
	net.SendToServer()
end

function tf2_pm.RequestUnsetBodygroup(bodygroup)
	net.Start('tf2_pm_request_bodygroup')
	net.WriteBool(false)
	net.WriteString(bodygroup)
	net.SendToServer()
end

function tf2_pm.RequestSetTeam(team_index)
	net.Start('tf2_pm_request_team')
	net.WriteUInt16(team_index)
	net.SendToServer()
end

function tf2_pm.RequestUnsetTeam()
	net.Start('tf2_pm_request_unteam')
	net.SendToServer()
end

function tf2_pm.RequestWearHat(hat)
	net.Start('tf2_pm_request_hat')
	net.WriteString(hat)
	net.SendToServer()
end

function tf2_pm.RequestRemoveHat(hat)
	net.Start('tf2_pm_request_unhat')
	net.WriteString(hat)
	net.SendToServer()
end

function tf2_pm.RequestRemoveAllHats()
	net.Start('tf2_pm_request_reset')
	net.SendToServer()
end

function tf2_pm.RequestSetHatColor(hat, color)
	net.Start('tf2_pm_request_color')
	net.WriteString(hat)
	net.WriteColor(color)
	net.SendToServer()
end

function tf2_pm.RequestUnsetHatColor(hat)
	net.Start('tf2_pm_request_uncolor')
	net.WriteString(hat)
	net.SendToServer()
end

function tf2_pm.RequestSetHatStyle(hat, style)
	net.Start('tf2_pm_request_style')
	net.WriteString(hat)
	net.WriteUInt16(style)
	net.SendToServer()
end

function tf2_pm.RequestUnsetHatStyle(hat)
	net.Start('tf2_pm_request_unstyle')
	net.WriteString(hat)
	net.SendToServer()
end

function tf2_pm.RequestReset()
	net.Start('tf2_pm_request_reset')
	net.SendToServer()
end

concommand.Add('tf2pm_require', function()
	net.Start('tf2_pm_request_all')
	net.SendToServer()
end)

concommand.Add('tf2pm_reload', function()
	local model = LocalPlayer():GetModel()
	local modeldata = tf2_pm.LookupModel(model)

	if not modeldata then return end

	if IsValid(tf2_pm.EditorWindow) and tf2_pm.EditorWindow.model:GetModel() == model then
		local localdef = LocalTF2PM()

		tf2_pm.RequestRemoveAllHats()

		local fromdef = tf2_pm.EditorWindow.model:GetTF2Data()

		for hat in pairs(fromdef.hat_defs) do
			tf2_pm.RequestWearHat(hat)
		end

		for hat, style in pairs(fromdef.hat_styles) do
			tf2_pm.RequestSetHatStyle(hat, style)
		end

		for hat, color in pairs(fromdef.hat_colors) do
			tf2_pm.RequestSetHatColor(hat, color)
		end

		if fromdef.team ~= localdef.team or fromdef.team_index ~= localdef.team_index then
			if fromdef.team then
				tf2_pm.RequestSetTeam(fromdef.team_index)
			else
				tf2_pm.RequestUnsetTeam()
			end
		end

		for name, value in pairs(localdef.custom_bodygroup_overrides) do
			if not fromdef.custom_bodygroup_overrides[name] then
				tf2_pm.RequestUnsetBodygroup(name)
			end
		end

		for name, value in pairs(fromdef.custom_bodygroup_overrides) do
			if localdef.custom_bodygroup_overrides[name] ~= value then
				tf2_pm.RequestSetBodygroup(name, value)
			end
		end
	else
		if not file.Exists('tf2pm/' .. modeldata.classname .. '/autoload.dat', 'DATA') then return end

		local localdef = LocalTF2PM()

		tf2_pm.RequestRemoveAllHats()

		localdef:DeserializeString(file.Read('tf2pm/' .. modeldata.classname .. '/autoload.dat', 'DATA'))

		for hat in pairs(localdef.hat_defs) do
			tf2_pm.RequestWearHat(hat)
		end

		for hat, style in pairs(localdef.hat_styles) do
			tf2_pm.RequestSetHatStyle(hat, style)
		end

		for hat, color in pairs(localdef.hat_colors) do
			tf2_pm.RequestSetHatColor(hat, color)
		end

		if localdef.team_index > 0 then
			tf2_pm.RequestSetTeam(localdef.team_index)
		else
			tf2_pm.RequestUnsetTeam()
		end

		tf2_pm.RequestRemoveAllBodygroups()

		for name, value in pairs(localdef.custom_bodygroup_overrides) do
			tf2_pm.RequestSetBodygroup(name, value)
		end
	end
end)

if IsValid(LocalPlayer()) then
	RunConsoleCommand('tf2pm_reload')
	RunConsoleCommand('tf2pm_require')
else
	hook.Add('InitPostEntity', 'TF2PM Local Data Init', function()
		RunConsoleCommand('tf2pm_reload')
		RunConsoleCommand('tf2pm_require')
	end)
end

net.receive('tf2_pm_request_reset', function()
	RunConsoleCommand('tf2pm_reload')
end)
