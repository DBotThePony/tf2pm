
-- Copyright (c) 2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.

gui.tf2pm.editor.title = 'TF2 PMM Editor'
gui.tf2pm.editor.title_modified = 'TF2 PMM Editor*'
gui.tf2pm.editor.hat_selector = 'Hat Selection'
gui.tf2pm.editor.hat_is_conflicting = 'This hat conflicts with current hat loadout'
gui.tf2pm.editor.cant_wear = "This hat cannot be worn under current conditions"
gui.tf2pm.editor.quicksearch = 'Quicksearch for hats'
gui.tf2pm.editor.quicksearch_file = 'Quicksearch for files'
gui.tf2pm.editor.filename = 'Filename'
gui.tf2pm.editor.model_mismatch = 'Your and editor\'s model are not the same, can\'t apply'
gui.tf2pm.editor.no_skin = 'No skin'
gui.tf2pm.editor.bodygroup_controls = 'Bodygroups'
gui.tf2pm.editor.default = '<default>'
gui.tf2pm.editor.nothing = '<nothing>'
gui.tf2pm.editor.skin_num = 'Skin #%d'

gui.tf2pm.editor.hat_control.unwear = 'Remove hat'
gui.tf2pm.editor.hat_control.choose_style = 'Choose style...'
gui.tf2pm.editor.hat_control.unset_style = 'Remove style'
gui.tf2pm.editor.hat_control.set_color = 'Choose color...'
gui.tf2pm.editor.hat_control.unset_color = 'Remove color'

gui.tf2pm.editor.hat_control.color_inspection = 'Inspecting hat color'
gui.tf2pm.editor.hat_control.style_inspection = 'Inspecting hat style'
gui.tf2pm.editor.hat_control.column_style = 'Style'
gui.tf2pm.editor.hat_control.choose_style = 'Choose style'
gui.tf2pm.editor.hat_control.style_num = 'Style %d'

gui.tf2pm.editor.load = 'Load this file'
gui.tf2pm.editor.remove = 'Remove this file'

gui.tf2pm.editor.load_button = 'Load'
gui.tf2pm.editor.save_button = 'Save'
gui.tf2pm.editor.clear_button = 'Clear'
gui.tf2pm.editor.clear_text = 'Are you sure you want to clear this hat loadout?'
gui.tf2pm.editor.save_text = 'Save this file as... (.dat extension not required)\nSave as autoload to make this file load automatically when you use this model.'

gui.tf2pm.hat_category.hats = 'Hats'
gui.tf2pm.hat_category.medals = 'Medals'
