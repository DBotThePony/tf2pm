
-- Copyright (c) 2020 DBotThePony

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.

gui.tf2pm.editor.title = 'TF2 PMM Редактор'
gui.tf2pm.editor.title_modified = 'TF2 PMM Редактор*'
gui.tf2pm.editor.hat_selector = 'Выбор шляпы'
gui.tf2pm.editor.hat_is_conflicting = 'Данная шляпа несовместима с текущим набором шляп'
gui.tf2pm.editor.cant_wear = "В данный момент эта шляпа не может быть надета"
gui.tf2pm.editor.quicksearch = 'Быстрый поиск шляп'
gui.tf2pm.editor.quicksearch_file = 'Быстрый поиск файлов'
gui.tf2pm.editor.filename = 'Имя файла'
gui.tf2pm.editor.model_mismatch = 'Модель вашего персонажа отличается от модели персонажа в редакторе'
gui.tf2pm.editor.no_skin = 'Без скина'
gui.tf2pm.editor.bodygroup_controls = 'Бадигрупы'
gui.tf2pm.editor.default = '<по умолчанию>'
gui.tf2pm.editor.nothing = '<ничего>'
gui.tf2pm.editor.skin_num = 'Скин №%d'

gui.tf2pm.editor.hat_control.unwear = 'Удалить шляпу'
gui.tf2pm.editor.hat_control.choose_style = 'Выбрать стиль...'
gui.tf2pm.editor.hat_control.unset_style = 'Убрать стиль'
gui.tf2pm.editor.hat_control.set_color = 'Выбрать цвет...'
gui.tf2pm.editor.hat_control.unset_color = 'Убрать цвет'

gui.tf2pm.editor.hat_control.color_inspection = 'Цвет шляпы'
gui.tf2pm.editor.hat_control.style_inspection = 'Стиль шляпы'
gui.tf2pm.editor.hat_control.column_style = 'Стиль'
gui.tf2pm.editor.hat_control.choose_style = 'Выбрать стиль'
gui.tf2pm.editor.hat_control.style_num = 'Стиль №%d'

gui.tf2pm.editor.load = 'Загрузить'
gui.tf2pm.editor.remove = 'Удалить'

gui.tf2pm.editor.load_button = 'Загрузить'
gui.tf2pm.editor.save_button = 'Сохранить'
gui.tf2pm.editor.clear_button = 'Очистить'
gui.tf2pm.editor.clear_text = 'Вы уверены что вы хотите снять все шляпы?'
gui.tf2pm.editor.save_text = 'Сохранить файл как... (рсширение .dat не нужно)\nСохраните как "autoload" (без кавычек) что бы загружать данный файл автоматически.'

gui.tf2pm.hat_category.hats = 'Шляпы'
gui.tf2pm.hat_category.medals = 'Медали'
