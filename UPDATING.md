# Updating TF2PM with latest TF2 items data

* Install/enable TF2PM (either from Workshop or from this Git repo)
* Make sure that TF2 is installed and mounted
* Load the game (any map and gamemode)
* Run the following command in console: `sv_allowcslua 1; lua_openscript_cl tf2pm_extractor.lua`
* After it finishes, go to `garrysmod/data/tf2pm_proc/output` folder, change extensions from `.txt` to `.lua` on all files; do the same for files in the `i18n` folder; use `ren *.txt *.lua` on Windows in folder for batch change extensions
* Copy all `.lua` files from `garrysmod/data/tf2pm_proc/output` into `lua/dlib/tf2pm/auto`
* Copy all `.lua` files from `garrysmod/data/tf2pm_proc/output/i18n` into `lua/dlib/tf2pm/lang`
* Change appropriate values in [`lua/dlib/autorun/tf2_pm.lua`](lua/dlib/autorun/tf2_pm.lua)
* The updated TF2PM copy only needs to be installed serverside. Clients will download the original addon with resources from Workshop automatically (WorkshopDL), so you are good to go from now. Optionally, you can fork and make a merge request to this Git repository with the updated data.
